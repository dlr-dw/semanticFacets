'use strict';
import Config         from    './config/config';
import queryValuesOne from    './queries/valuesOne';
import queryValuesTwo from    './queries/valuesTwo';
import Value          from    './objects/value';
import clusterNumeric from    './cluster/numeric';
import clusterDate    from    './cluster/date';
/**
 * for the given list of IRIs retrieve all values (IRI, label, count) for the given facets
 *
 * @param   {Function}        conn          wrapper function to issue queries through
 * @param   {Array.String}    iris          list of IRIs under consideration
 * @param   {Array.Facet}     facet         the facets to augment
 * @returns
 */
export default async function populateValues( conn, iris, facets ){

  // split by path length
  const pathLengthOne = [], pathLengthTwo = [];
  for( const facet of facets ) {
    if( facet.path.length == 1 ) {
      pathLengthOne.push( facet );
    } else {
      pathLengthTwo.push( facet );
    }
  }

  // split path lengths of two into clusters of same base property
  const byBasePath = {};
  for( const facet of pathLengthTwo ) {
    const base = facet.path[0].iri;
    byBasePath[ base ] = byBasePath[ base ] || [];
    byBasePath[ base ].push( facet );
  }

  // augment all facets with queries
  await Promise.all([
    processSingleStep( conn, iris, pathLengthOne ),
    ... Object.values( byBasePath ).map( (group) => processMultiStep( conn, iris, group ) )
  ]);

  // some facets' values may need to be clustered
  for( const facet of facets ){

    switch( facet.datatype ) {

      case 'http://www.w3.org/2001/XMLSchema#decimal':
        facet.values = clusterNumeric( facet.values, Config.bucketCount );
        break;

      case 'http://www.w3.org/2001/XMLSchema#dateTime':
        facet.values = clusterDate( facet.values, Config.bucketCount );
        break;

    }

  }

}


/**
 * parse query results into Value objects
 *
 * @returns   {Object}    mapping between path and the list of Value objects for that path
 */
function parseValues( resp ) {

  const result = {};
  for( const el of resp ) {

    // parse into value object
    const val = !('value' in el)
                  // "unknown" placeholder
                  ? new Value( null, Config.unkownLabel, el.valueSize )
                  // "normal" value
                  : new Value( el.value, el.value, el.valueSize );

    // put into the respective group
    result[ el.path ] = result[ el.path ] || [];
    result[ el.path ].push( val );

  }

  return result;

}


/**
 * process facets with single step paths
 */
async function processSingleStep( conn, iris, facets ) {

  // issue query
  const propPaths = facets.map( (f) => { return { type: 'iri', value: f.path[0].iri }; } ),
        query     = queryValuesOne( iris, propPaths ),
        resp      = await conn( query );

  // parse the values
  const values = await parseValues( resp );

  // assign to the respective facets
  for( const facet of facets ) {
    facet.values = values[ facet.path[0].iri ];
  }

}


/**
 * process facets with step paths of length two
 */
async function processMultiStep( conn, iris, facets ) {

  // issue query
  const propPaths = facets.map( (f) => { return { type: 'iri', value: f.path[1].iri }; } ),
        base      = { type: 'iri', value: facets[0].path[0].iri },
        query     = queryValuesTwo( iris, base, propPaths ),
        resp      = await conn( query );

  // parse the values
  const values = await parseValues( resp );

  // assign to the respective facets
  for( const facet of facets ) {
    facet.values = (facet.path[1].iri in values) ? values[ facet.path[1].iri ] : [];
  }

}
