'use strict';
/**
 * wrap a simple property and its associated entity
 */
export default class Property {

  constructor( iri ) {
    this._iri       = '' + iri;
    this._entity    = '';
    this._label     = '';
    this._ancestors = null;
  }


  get iri() {
    return this._iri;
  }
  set iri( val ) {
    throw new Error( 'IRI is immutable.' );
  }


  get entity() {
    return this._entity;
  }
  set entity( val ) {
    this._entity = '' + val;
  }


  get label() {
    return this._label;
  }
  set label( val ) {
    this._label = '' + val;
  }


  get ancestors() {
    return this._ancestors;
  }
  set ancestors( val ) {
    this._ancestors = val;
  }

}
