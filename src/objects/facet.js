'use strict';
/*
 * simple object to hold all the values for a facet (candidate)
 *
 * - no functionality, but datatype/range checks
 * - value IRIs are required to be distinct (aka no duplicate values)
 */

import Property from './property';

// map from score name to position in array
// allows to define multiple aliases for scores
// name in all lower case per definition
import scoreDef from '../config/scores';
const SCORE_COUNT = scoreDef.length,
      SCORE_MAP   = scoreDef.reduce( (all, el, ind) => {
        const aliases = el.alias.reduce( (all,el) => {
          return {
            ... all,
            [el]: ind,
          };
        }, {} );
        return {
          ... all,
          ... aliases,
        };
      }, {} );

export default class Facet {

  /**
   * @param {String|Array.String}   properties    property path associated with this facet as list of IRIs
   * @param {String}                datatype      datatype used for values in this property path
   */
  constructor( path, datatype ) {

    // property path
    if( !(path instanceof Array) ) {
      path = [ path ];
    }
    path = path.map( (el) => new Property( el ) );
    this._path = Object.freeze( path );

    // scores cache
    this._scores = (new Array( SCORE_COUNT )).fill( -1 );

    // init other instance properties
    this._dt      = datatype;
    this._values  = null;

  }


  get datatype() {
    return this._dt;
  }
  set datatype( val ){
    throw new Error( 'Datatype is immutable.' );
  }


  get path() {
    return this._path;
  }
  set path( val ){
    throw new Error( 'Property paths are immutable.' );
  }


  get values() {
    return this._values ? [ ... this._values ] : null;
  }
  set values( val ) {
    if( val instanceof Array ) {
      this._values = new Set( val );
    } else {
      this._values = new Set( [ val ] );
    }
  }


  /**
   * attach a single value to this facet
   * @param {Value}   val       the value to be added
   */
  addValue( val ){

    // checks
    if( this._values ) {
      if( !this._values.has( val ) ) {

        // remove duplicate, if existing
        const existing = [ ... this._values ].find( (el) => el.iri == val.iri );
        if( existing ) {
          this._values.delete( existing );
        }

      }
    } else {
      this._values = new Set();
    }

    // add new item
    this._values.add( val );

    return this;
  }

  /**
   * @param {String}    name    name of the intra-facet score to retrieve
   */
  getScore( name ) {

    // check name
    if( !(name in SCORE_MAP) ) {
      throw new Error( 'Unknown score.' );
    }

    return this._scores[ SCORE_MAP[name] ];

  }

  /**
   * @param {String}  name    name of the intra-facet score to store
   * @param {Number}  value   the scores actual value
   */
  setScore( name, value ){

    // check name
    if( !(name in SCORE_MAP) ) {
      throw new Error( 'Unknown score.' );
    }

    // scores need to be in [0,1]
    if( !( (value >= 0) && (value <= 1 ) ) ) {
      throw new Error( `Invalid range for score (${name}): ${value}` );
    }

    this._scores[ SCORE_MAP[name] ] = value;

    return this;

  }
}
