'use strict';
import Value    from    './value';

/**
 * wrap a range of numeric values
 */
export default class RangeValueNumeric extends Value {

  /**
   * @param   {Number}      min         minimum value included
   * @param   {Number}      max         maximum value included
   * @param   {Number}      count       number of occurrences
   */
  constructor( min, max, count ) {

    // call parent constructor
    super( null, `${min} - ${max}`, count );

    // store values
    this.min = min | 0;
    this.max = max | 0;

  }

}
