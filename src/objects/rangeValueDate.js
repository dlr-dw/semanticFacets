'use strict';
import Value    from    './value';

/**
 * wrap a range of numeric values
 */
export default class RangeValueDate extends Value {

  /**
   * @param   {Number}      min         minimum date included
   * @param   {Number}      max         maximum date included
   * @param   {Number}      count       number of occurrences
   */
  constructor( min, max, count ) {

    // call parent constructor
    super( null, `${min} - ${max}`, count );

    // store values
    this.min = min;
    this.max = max;

  }

}
