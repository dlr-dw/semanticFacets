'use strict';
/*
 * wrap a facet value
 */
export default class Value {

  /**
   * @param {String}    iri           the value's IRI
   * @param {String}    label         a label for this value
   * @param {Number}    count         number of occurrences for this value
   */
  constructor( iri, label, count ) {

    this._iri   = iri;

    this._label = '' + label;

    if( !Number.isInteger( count ) ) {
      throw Error( `Count needs to be an integer value. was: "${count}" (${typeof count})` );
    }
    this._count = +count | 0;

  }


  get iri() {
    return this._iri;
  }
  set iri( val ) {
    throw new Error( 'URIs are immutable' );
  }


  get label() {
    return this._label;
  }
  set label( val ) {
    this._label = '' + val;
  }


  get count() {
    return this._count;
  }
  set count( val ) {
    if( !Number.isInteger( val ) ) {
      throw Error( 'Count needs to be an integer value' );
    }
    this._count = val;
  }

}
