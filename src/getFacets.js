'use strict';
import Config               from './config/config';
import Scores               from './config/scores';
import generateCandidates   from './generateCandidates';
import populateValues       from './populateValues';
import addLabels            from './addLabels';
import finalScore           from './scores/final';
import semanticScore        from './scores/semanticSimilarity';
import prepareSemSim        from './scores/semanticSimilarity/prepare';
/**
 * Generate a list of suitable facets for the given list of IRIs
 *
 * @param     {Function}        conn            wrapper function to issue queries through
 * @param     {Array.String}    inputIris       list of input IRIs
 * @param     {Number}          facetCount      number of facets to be returned
 * @param     {Function}        [lifecycleCB]   lifecycle callback used for measuring the overall performance
 * @returns   {Array.Facet}                     list of facets generated that were deemed most useful
 */
export default async function getFacets( conn, inputIris, facetCount, lifecycleCB ) {

  // set a dummy lifecycle callback, so we do not have to check over and over again later
  lifecycleCB = (lifecycleCB instanceof Function) ? lifecycleCB : () => {};
  lifecycleCB( 'start' );

  // make sure we have a list of distinct IRIs
  if( !(inputIris instanceof Array) || inputIris.some( (el) => typeof el !== 'string' ) ) {
    throw Error( 'Parameter needs to be a list of IRIs' );
  }
  const iris = new Set( inputIris );

  // convert to IRI objects that the SPARQl queries will understand
  const formatedIRIs = [ ... iris ].map( (iri) => {
    return {
      type: 'iri',
      value: iri,
    };
  });

  // get facet candidates; includes direct and indirect properties
  lifecycleCB( 'beforeCandidates' );
  let candidates = await generateCandidates( conn, formatedIRIs, Config.minPredProb );
  lifecycleCB( 'afterCandidates' );

  // populate values for facets
  lifecycleCB( 'beforeValues' );
  await populateValues( conn, formatedIRIs, candidates );
  lifecycleCB( 'afterValues' );

  // compute intra-facet scores
  lifecycleCB( 'beforeIntraScoring' );
  for( const score of Scores ) {
    if( score.funktion && !score.final ){
      for( const cand of candidates ) {
        score.funktion( cand );
      }
    }
  }

  // compute the final intra-facet scoring
  for( const cand of candidates ) {
    finalScore( cand );
  }
  lifecycleCB( 'afterIntraScoring' );

  // sort by final score by descending final score
  candidates.sort( (a,b) => b.getScore( 'final' ) - a.getScore( 'final' ) );

  // select top-entry per direct property ("better categorization")
  // list is sorted, so while traversing we should encounter the best facet for this direct property first
  // discard any later appearances
  const selectedProp = new Set();
  candidates = candidates.filter( (facet) => {
    if( !selectedProp.has( facet.path[0] ) ) {
      selectedProp.add( facet.path[0] );
      return true;
    } else {
      return false;
    }
  });

  // if all candidates are requested, we don't need to go on
  if( facetCount < 0 ) {
    return candidates;
  }

  // select top-k facets based on inter-facet ranking
  lifecycleCB( 'beforeInterScoring' );
  const result = [],
        symPrepare = Symbol.for( 'promise for preparing the respective facet' );
  let runner = 0;
  candidates[0][ symPrepare ] = prepareSemSim( conn, candidates[0] );
  while( (result.length < facetCount) && (runner < candidates.length) ){

    // trigger preparation of next candidate already
    candidates[ runner ][ symPrepare ] = prepareSemSim( conn, candidates[ runner ] );

    // shortcut
    const cand = candidates[ runner ];

    // wait until this candidate is prepared
    await cand[ symPrepare ];

    // compare to all select candidates
    lifecycleCB( 'beforeStepSimilarity' );
    const semSimilarity = await semanticScore( conn, result, cand );
    lifecycleCB( 'afterStepSimilarity' );

    // add to result
    if( semSimilarity < Config.semanticSimilarity ) {
      result.push( cand );
    }

    // next iteration
    runner += 1;

  }
  lifecycleCB( 'afterInterScoring' );

  // add labels to properties and values
  lifecycleCB( 'beforeLabels' );
  await addLabels( conn, result );
  lifecycleCB( 'afterLabels' );


  lifecycleCB( 'end' );
  return result;

}
