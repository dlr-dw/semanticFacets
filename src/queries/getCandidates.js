'use strict';
import tagSparql from './../util/tagSparql';

/**
 * Collect facet candidates
 * will already filter out those, that are too rare to be used
 *
 * @param     {Array}           iris        list of IRIs to examined
 * @param     {Number}          minProp     minimum probability
 * @returns
 */
export default function getCandidates( iris, minProb ) {

  return tagSparql`
## query: getCandidates
SELECT (?directProp1 AS ?prop1)

       (COUNT(DISTINCT ?iri)      AS ?withProp)
       (?withProp/${iris.length}  AS ?probScore)
       (DATATYPE(SAMPLE(?value2)) AS ?dt)
WHERE {

  hint:Query hint:optimizer "None" .

  # input
  VALUES ?iri { ${iris} }

  # collect properties
  ?iri       ?directProp1               ?value2.
  ?property1 wikibase:directClaim       ?directProp1.
  FILTER NOT EXISTS { ?property1 wdt:P31/wdt:P279* wd:Q19847637} .
  FILTER NOT EXISTS { ?property1 wdt:P31/wdt:P279* wd:Q51118821} .

}
GROUP BY ?directProp1
HAVING (
  # sufficiently often
  (?probScore >= ${minProb})
  &&
  # no string types
  ( !BOUND(?dt) || (?dt NOT IN ( rdf:langString, xsd:string, geo:wktLiteral ) ) )
)
  `;
}
