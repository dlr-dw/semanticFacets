'use strict';
import tagSparql from './../util/tagSparql';

/**
 * Collect facet candidates from indirect properties
 * will already filter out those, that are too rare to be used
 *
 * @param     {Array}           iris        list of IRIs to examined
 * @param     {Array}           props       direct properties to consider
 * @param     {Number}          minProp     minimum probability
 * @returns
 */
export default function getCandidates_indirect( iris, props, minProb ) {

  return tagSparql`
## query: getCandidates_indirect
SELECT (?directProp1 AS ?prop1)
       (?directProp2 AS ?prop2)

       (COUNT(DISTINCT ?iri)      AS ?withProp)
       (?withProp/${iris.length}  AS ?probScore)
       (DATATYPE(SAMPLE(?value2)) AS ?dt)
WHERE {

  hint:Query hint:optimizer "None" .

  # input
  VALUES ?iri { ${iris} }
  VALUES ?directProp1 { ${props} }

  # collect properties
  ?iri       ?directProp1         ?value1.
  {
    SELECT *
    WHERE {
      ?value1     ?directProp2 ?value2.
      ?property2  wikibase:directClaim ?directProp2.
      FILTER NOT EXISTS { ?property2 wdt:P31/wdt:P279* wd:Q19847637} .
      FILTER NOT EXISTS { ?property2 wdt:P31/wdt:P279* wd:Q51118821} .
    }
  }

}
GROUP BY ?directProp1 ?directProp2
HAVING (
  # sufficiently often
  (?probScore >= ${minProb})
  &&
  # no string types
  ( !BOUND(?dt) || (?dt NOT IN ( rdf:langString, xsd:string, geo:wktLiteral ) ) )
)
  `;
}
