'use strict';
import tagSparql from './../util/tagSparql';

/**
 * Collect facet candidates from property-paths of fixed length
 * will already filter out those, that are too rare to be used
 *
 * @param     {Array}           iris        list of IRIs to examined
 * @param     {Array.Array}     props       property paths to consider
 * @param     {Number}          len         property path length
 * @param     {Number}          minProp     minimum probability
 * @returns
 */
export default function getCandidates_indirect2( iris, props, len, minProb ) {

  /* prepare query parts */

  // variable list (input)
  const variables = Array( len )
    .fill( 1 )
    .map( (el,i) => `?directProp${i+1}` );

  // variable list (all)
  const allVariables = [ ... variables, `?directProp${len+1}` ];

  // select statement
  const select = {
    type:   'raw',
    value:  allVariables
      .map( (el,i) => `(${el} AS ?prop${i+1})` )
      .join( ' ' )
  };

  // input values statement
  const values = {
    type:   'raw',
    value:  props
      .map( (set) => tagSparql`( ${set} )` )
      .join( ' ' )
  };

  // input bound variables
  const inputVariables = {
    type:   'raw',
    value:  '(' + variables.join( ' ' ) + ')'
  };

  // property path
  const path = {
    type:   'raw',
    value:  variables
      .slice( 1 ) // skip the first, as it is hardcoded in the query
      .map( (v,i) => `?value${i+1} ${v} ?value${i+2} .` ).join( ' ' )
  };

  // considered value at the end of the property path
  const finalValue = {
    type:   'raw',
    value:  `?value${variables.length}`
  };

  // filter duplicates in the property chain
  const filter = {
    type:   'raw',
    value:  (function(){
      const res = [];
      for( let i=1; i<variables.length; i++ ) {
        for( let j=i+1; j<=variables.length; j++ ) {
          res.push( `FILTER (?value${i} != ?value${j})` );
        }
      }
      return res.join( '\n' );
    })(),
  };

  // final property (new ones detected by this query)
  const finalProp = {
    type: 'raw',
    value: `?directProp${len+1}`
  };

  // group by statement
  const groupBy = {
    type: 'raw',
    value:  allVariables.join( ' ' )
  };

  return tagSparql`
## query: getCandidates_indirect2
SELECT ${select}

       (COUNT(DISTINCT ?iri)      AS ?withProp)
       (?withProp/${iris.length}  AS ?probScore)
       (DATATYPE(SAMPLE(?valueX)) AS ?dt)
WHERE {

  hint:Query hint:optimizer "None" .

  # input
  VALUES ?iri { ${iris} }
  VALUES ${inputVariables} { ${values} }

  # collect properties
  ?iri ?directProp1 ?value1 .
  ${path}

  # filter duplicates within a property chain
  ${filter}

  {
    SELECT *
    WHERE {
      ${finalValue} ${finalProp} ?valueX.
      ?property2    wikibase:directClaim ${finalProp}.
      FILTER NOT EXISTS { ?property2 wdt:P31/wdt:P279* wd:Q19847637} .
      FILTER NOT EXISTS { ?property2 wdt:P31/wdt:P279* wd:Q51118821} .
    }
  }

}
GROUP BY ${groupBy}
HAVING (
  # sufficiently often
  (?probScore >= ${minProb})
  &&
  # no string types
  ( !BOUND(?dt) || (?dt NOT IN ( rdf:langString, xsd:string, geo:wktLiteral ) ) )
)
  `;
}
