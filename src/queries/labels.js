'use strict';
import tagSparql from './../util/tagSparql';

/**
 * retrieve labels for the given IRIs
 *
 * @param     {Array.Object}    iris        list of IRIs
 * @returns
 */
export default function labels( iris ) {

  return tagSparql`
## query: labels
SELECT ?iri
       ?iriLabel
WHERE {

  # input
  VALUES ?iri { ${iris} }

  # label service
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en" .
  }

}
  `;
}
