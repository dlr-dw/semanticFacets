'use strict';
import tagSparql from './../util/tagSparql';

/**
 * retrieve labels for the given IRIs (property edition)
 *
 * @param     {Array.Object}    iris        list of property-IRIs
 * @returns
 */
export default function labels( iris ) {

  return tagSparql`
## query: labelsProp
SELECT ?prop
       ?iri
       ?iriLabel
WHERE {

  # input
  VALUES ?prop { ${iris} }

  # link to the entity
  ?prop ^wikibase:directClaim ?iri .

  # label service
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en" .
  }

}
  `;
}
