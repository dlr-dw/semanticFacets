'use strict';
import tagSparql from './../util/tagSparql';

/**
 * retrieve all (distinct) values from the SPARQL endpoint and they respective counts
 * for the given IRIs and property path of length one
 *
 * @param     {Array.Object}    iris        list of IRIs
 * @param     {Array.Object}    paths       list of paths
 * @returns
 */
export default function values( iris, paths ) {

  return tagSparql`
## query: values_one
SELECT ?path
       ?value
       (COUNT(DISTINCT ?iri) AS ?valueSize)
WHERE {

  # input
  VALUES ?iri   { ${iris} }
  VALUES ?path  { ${paths} }

  # get values
  OPTIONAL { ?iri ?path ?value }
  FILTER( !BOUND(?value) || !isBLANK(?value) )

} GROUP BY ?path ?value
  `;
}
