'use strict';
import tagSparql from './../util/tagSparql';

/**
 * returns a list of _direct_ ancestors in the graph wrt to the following properties
 * - P279   subclass of
 * - P31    instance of
 *
 * @param     {String}          entity      entity in question
 * @returns
 */
export default function ancestors_direct( entity ) {

  return tagSparql`
## query: ancestors_direct
SELECT ?parent
WHERE {
  ${entity} (wdt:P31|wdt:P279) ?parent .
}
  `;
}
