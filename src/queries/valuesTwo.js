'use strict';
import tagSparql from './../util/tagSparql';

/**
 * retrieve all (distinct) values from the SPARQL endpoint and they respective counts
 * for the given IRIs and property path of length two
 *
 * @param     {Array.Object}    iris        list of IRIs
 * @param     {Object}          base        common base path of all elements
 * @param     {Array.Object}    paths       list of paths
 * @returns
 */
export default function values( iris, base, paths ) {

  return tagSparql`
## query: values_two
SELECT ?path
       ?value
       (COUNT(DISTINCT ?iri) AS ?valueSize)
WHERE {

  # input
  VALUES ?iri   { ${iris} }
  VALUES ?path  { ${paths} }

  # get values
  OPTIONAL { 
    ?iri ${base} ?x .
    ?x ?path ?value .
  }
  FILTER( !BOUND(?value) || !isBLANK(?value) )

} GROUP BY ?path ?value
  `;
}
