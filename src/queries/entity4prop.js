'use strict';
import tagSparql from './../util/tagSparql';

/**
 * retrieve the corresponding entity for a given list of property
 *
 * @param     {Array.String}          prop     the property in question
 * @returns
 */
export default function enitityForProperty( prop ) {

  // list of properties - SPARQL formatted
  const propList = {
    value: prop.map( (p) => tagSparql`${p}` ).join( ' ' ),
    type:  'raw',
  };

  return tagSparql`
## query: entity4prop
SELECT  (?input AS ?propIri)
        ?entity
        ?propLabel
WHERE {

  # input
  VALUES ?input { ${propList} }

  # describing entity
  ?input ^wikibase:directClaim ?prop .
  ?prop  wdt:P1629 ?entity .

}
  `;
}
