'use strict';
import tagSparql from './../util/tagSparql';

/**
 * returns a list of ancestors in the graph wrt to the following properties
 * - P279   subclass of
 * - P31    instance of
 *
 * Note:
 * The same query for multiple entities using a VALUES statement is far slower than issuing individual queries!
 *
 * @param     {String}          entity      entity in question
 * @returns
 */
export default function ancestors( entity ) {

  return tagSparql`
## query: ancestors
SELECT ?middle ?parent
WHERE {
  ${entity} (wdt:P31|wdt:P279)* ?middle .
  ?middle   (wdt:P31|wdt:P279)  ?parent .
}
  `;
}
