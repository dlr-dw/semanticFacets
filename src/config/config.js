export default {

  // endpoint to query
  endpoint:     'https://query.wikidata.org/sparql',

  // maximum parallel queries
  maxParallelQueries:   5,

  // minimum predicate probability to be considered a candidate facet
  minPredProb:  0.1,

  // threshold for semantic similarity
  // below means included
  semanticSimilarity: 0.7,

  // label for "unknown" value
  unknownLabel: 'unknown',

  // number of buckets for continuous properties
  bucketCount:  10,

};
