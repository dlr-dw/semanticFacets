'use strict';
/**
 * configure scores to be used
 *
 * - final is included to allow objects/facet a complete mapping to the array elements
 */

//@TODO should be replaced with dynamic loading, when it comes online
import valueCardinality   from './../scores/valueCardinality';
import valueDispersion    from './../scores/valueDispersion';

export default [

  {
    name:     'Final Aggregate Score',
    alias:    [ 'final' ],
    file:     'scores/final.js',
    funktion: null,
    final:    true,
    weight:   0,
  },

  {
    name:     'Predicate Occurrence Propability',
    alias:    [ 'probability' ],
    file:     null,
    funktion: null,
    final:    false,
    weight:   0.5,
  },

  {
    name:     'Value Cardinality',
    alias:    [ 'cardinality' ],
    file:     'scores/valueCardinality.js',
    funktion: valueCardinality,
    final:    false,
    weight:   0.33,
  },

  {
    name:     'Value Dispersion',
    alias:    [ 'dispersion' ],
    file:     'scores/valueDispersion.js',
    funktion: valueDispersion,
    final:    false,
    weight:   0.17,
  },

];
