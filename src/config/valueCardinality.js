'use strict';
/**
 * settings for scores/valueCardinality
 */
export default {

  // number of facet-values considered optimal
  optimum: 10,

  // minimal number of facet values
  minimum:  2,

  // determine amount of punishment for deviation from optimum
  alpha:    3,

};
