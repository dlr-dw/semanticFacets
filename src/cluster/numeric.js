'use strict';
import    RangeValue      from    './../objects/rangeValueNumeric';
/**
 * cluster the given list of values in the given number of buckets
 *
 * - labels will be converted to actual numbers
 * - number of buckets will only be respected, if there are enough values
 * - buckets will be evenly sized in terms of values included
 *
 * @param   {Array.Value}       values          input values; label should be numeric
 * @param   {Number}            bucketCount     number of buckets to be returned
 * @returns {Array.Value}                       a clustered list of values
 */
export default function clusterNumeric( values, bucketCount ) {

  // an "unknown" value is left untouched
  const unkown = values.find( (v) => v.iri === null );

  // now we only care about those that actually have a value
  values = values.filter( (v) => v.iri !== null );

  // attach parsed values
  const symParsed = Symbol.for( 'parsed value' );
  values.forEach( (v) => v[symParsed] = parseFloat( v.label ) );

  // sort by label (== ISO date)
  values.sort( (a,b) => a[symParsed] - b[symParsed] );

  // split into buckets
  // buckets are first evenly sized; remaining slots are assigned to the first buckets
  bucketCount -= unkown ? 1 : 0; // account for the "unknown" bucket, if existing
  const bucketMinSize   = Math.trunc( values.length / bucketCount ),
        bucketMaxSize   = bucketMinSize + 1,
        overfullBuckets = values.length % bucketCount,
        result          = [];
  for( let i=0; i<values.length; i += (result.length <= overfullBuckets) ? bucketMaxSize : bucketMinSize ) {

    // get the subset of values
    const subset = (result.length < overfullBuckets)
                    ? values.slice( i, i + bucketMaxSize )
                    : values.slice( i, i + bucketMinSize );

    // get overall count
    const subsetCount = subset.reduce( (acc,el) => acc + el.count, 0 );

    // create a range object and add to result
    const range = new RangeValue( subset[0][symParsed], subset[ subset.length - 1 ][symParsed], subsetCount );
    result.push( range );

  }

  // add the "unknown" bucket, if existing
  if( unkown ) {
    result.unshift( unkown );
  }

  // done
  return result;

}
