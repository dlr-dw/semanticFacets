'use strict';
import getAncestors           from    './semanticSimilarity/getAncestors';

// configuration parameters
const ALPHA = 0.2,
      BETA  = 0.6;

// cache depth of LCS nodes
const lcsDepthCache = {};

/**
 * score the semantic similarity between the given facet and the list of currently used results
 * range from 0 (not similar) to 1 (similar)
 *
 * implements Scenario 4 from
 * Yuhua Li, Zuhair A. Bandar, and David McLean, An Approach for Measuring Semantic Similarity between Words Using Multiple Information Sources
 *
 * @param   {Function}        conn          wrapper function to issue queries through
 * @param   {Array.Facet}     bulk          list of facets to compare to
 * @param   {Facet}           facet         candidate facet to be evaluated
 * @returns {Number}                        similarity score
 */
export default async function semanticSimilarity( conn, bulk, facet ) {

  // if there is no entity for the facet, we can just end here
  const prop = facet.path[0];
  if( !prop.entity ) {
    return 0;
  }

  // add the current facet depth to our cache
  // will save some queries, if at some point an entity is the direct parent of another
  lcsDepthCache[ prop.entity ] = Math.max( ... Object.values( prop.ancestors ) );

  // score against all facets in bulk
  const ancestors = prop.ancestors;
  const scores = bulk.map( async (otherFacet) => {

    // shortcut(s)
    const otherProp = otherFacet.path[ 0 ];
    const otherAncestors = otherProp.ancestors;

    // get the least common subsumer (LCS)
    const commonAnc = Object.keys( ancestors ).filter( (a) => (a in otherAncestors) ),
          lcs       = commonAnc.reduce( (lcs, anc) =>
          ( (anc in otherAncestors) && (ancestors[ anc ] < ancestors[ lcs ]) )
            ? anc
            : lcs
          , commonAnc[0] );

    // if no common ancestor is found, we can stop here
    if( !lcs ) {
      return 0;
    }

    // get depth of LCS
    if( !(lcs in lcsDepthCache) ) {
      lcsDepthCache[ lcs ] = (async () => {
        const lcsAncestors = await getAncestors( conn, lcs );
        return Math.max( ... Object.values( lcsAncestors ) );
      })();
    }
    const lcsDepth = await lcsDepthCache[ lcs ];

    // calculate the overall score
    const exp     = BETA * lcsDepth,
          ePlus   = Math.pow( Math.E, exp ),
          eMinus  = Math.pow( Math.E, -exp );
    return   Math.pow( Math.E, - ALPHA * (ancestors[ lcs ] + otherAncestors[ lcs ]) )
           * (ePlus - eMinus) / (ePlus + eMinus);

  });

  // return the minimum score
  if( scores.length < 1 ) {
    return 0;
  } else {
    const resolvedScores = await Promise.all(scores);
    return Math.max( ... resolvedScores );
  }

}
