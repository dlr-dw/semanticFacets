'use strict';
/**
 * compute final aggregated score using weighted average
 *
 * - score is added to facet
 */

// prepare scoring
import Scores from '../config/scores';
const usedScores = Scores.filter( (s) => !s.final && (s.weight > 0) );

export default function final( facet ) {

  // calculate final score
  const score = usedScores.reduce( (acc,el) => acc + facet.getScore( el.alias[0] ) * el.weight, 0 );

  // set score in facet
  facet.setScore( 'final', score );

}
