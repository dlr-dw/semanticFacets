'use strict';
/**
 * compute value dispersion score for the given facet
 *
 * - score is added to facet
 */

export default function valueDispersion( facet ) {

  // make sure values are populated
  if( facet.values === null ) {
    throw new Error( 'No values attached to facet.' );
  }

  // exclude the unknown value
  const values = facet.values.filter( (v) => v.iri != null );

  // no values is a score of 0
  if( values.length < 1 ) {
    facet.setScore( 'dispersion', 0 );
    return;
  }

  // calculate mean number of resources per value
  const total = values.reduce( (all,value) => all + value.count, 0 ),
        mean  = total / values.length;

  // calculate standard deviation of resources per value
  let std = 0;
  for( const value of values ) {
    std += Math.pow( value.count - mean, 2 );
  }
  std = Math.sqrt( std );

  // calculate coefficient variation
  const cv = std / mean;

  // calculate final score
  const score = 1 / (1 + cv);

  // set score in facet
  facet.setScore( 'dispersion', score );

}
