'use strict';
import queryEntities          from    './../../queries/entity4prop';
import getAncestors           from    './getAncestors';

/**
 * prepare a given facet, so we can compute the semantic similarity to other facets
 *
 * @param   {Function}  conn      wrapper function to issue queries through
 * @param   {Facet}     prop      the property to prepare
 * @returns {Facet}               the same property augmented with the necessary information
 */
export default async function prepare( conn, facet ) {

  // populate entities for the facet
  // TODO cope with multiple entities for a property
  const properties = facet.path
    .filter( (p) => !p.entity )
    .map( (p) => {
      return {
        value: p.iri,
        type:  'iri',
      };
    });
  if( properties.length > 0 ) {
    const formatedQuery = queryEntities( properties ),
          resp          = await conn( formatedQuery );
    for( const row of resp ) {
      const prop = facet.path.find( (p) => p.iri == row.propIri );
      prop.entity = row.entity;
    }
  }

  // if there is no entity for the facet, we can just end here
  const prop = facet.path[0];
  if( !prop.entity ) {
    return facet;
  }

  // create a list of all ancestors for the property including their respective distance
  // caching should not improve here, as every entity should only appear once due to "better categorization"
  prop.ancestors = await getAncestors( conn, prop.entity );

  return facet;

}
