'use strict';
import queryAncestors         from    './../../queries/ancestors';
import queryAncestorsDirect   from    './../../queries/ancestors_direct';

/**
 * get the ancestor list for the given IRI, i.e., all respective superclasses
 *
 * @param   {Function}    conn      wrapper function to issue queries through
 * @param   {Property}    iri       the base IRI
 */
export default async function getAncestors( conn, iri ) {

  // initialize the result with the IRI in question
  const result = {
    [iri]: 0,
  };

  // retrieve ancestor list for direct and indirect ancestors
  const reqs = [];
  let formatedQuery = queryAncestors({ type: 'iri', value: iri });
  reqs.push( conn( formatedQuery ) );
  formatedQuery = queryAncestorsDirect({ type: 'iri', value: iri });
  reqs.push( conn( formatedQuery ) );
  await Promise.all( reqs );
  const ancestors       = await reqs[0],
        directAncestors = new Set( (await reqs[1]).map( (row) => row.parent ) );

  // get the current frontier, ie., start with the direct parents (and their connections)
  const frontier = ancestors.filter( (row) => directAncestors.has( row.middle ) );
  directAncestors.forEach( (anc) => result[ anc ] = 1 );

  // if there are no indirect ancestors, we can stop here
  if( ancestors.length < 1 ) {
    return result;
  }

  // build a graph-like structure
  const graph = {};
  for( const row of ancestors ) {
    graph[ row.middle ] = graph[ row.middle ] || new Set();
    graph[ row.middle ].add( row );
  }

  // collect all ancestors along with their distance to prop.iri
  let distance = 2; // starting frontier is distance 1
  while( frontier.length > 0 ) {

    // process the old frontier and retrieve a new one
    const outerSpace = new Set();
    for( const node of frontier ) {

      // if the parent was already added, we had a shorter path to it
      // hence we do not need to process it again
      if( !(node.parent in result) ) {

        // enqueue for next iteration, if there are further ancestors
        if( node.parent in graph ) {
          graph[ node.parent ].forEach( (row) => outerSpace.add( row ) );
        }

        // set distance
        result[ node.parent ] = distance;

      }

    }

    // prepare next iteration
    frontier.length = 0;
    frontier.push( ... outerSpace );
    distance += 1;

  }

  // attach result to property
  return result;

}
