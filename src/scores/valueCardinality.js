'use strict';
/**
 * compute value cardinality score for the given facet
 *
 * - score is added to facet
 */

import Settings from '../config/valueCardinality';

export default function valueCardinality( facet ) {

  // make sure values are populated
  if( facet.values === null ) {
    throw new Error( 'No values attached to facet.' );
  }

  // answer is defined as partial function based on number of values
  const valueCount = facet.values.length;
  let score;
  switch( true ) {

    case (valueCount < Settings.minimum):
      score = 0;
      break;

    case (valueCount < Settings.optimum):
      score = Math.exp( (valueCount - Settings.optimum) / (Settings.alpha * Settings.alpha) );
      break;

    default:
      score = 1 / ( 1 + (valueCount - Settings.optimum) );
      break;

  }

  // set score in facet
  facet.setScore( 'cardinality', score );

}
