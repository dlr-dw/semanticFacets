'use strict';

import Facet                      from './objects/facet';
import queryCandidates            from './queries/getCandidates';
import queryCandidates_indirect   from './queries/getCandidates_indirect';

/**
 * generate candidate facets for the given IRIs
 *
 * @param     {Function}        conn        wrapper function to issue queries through
 * @param     {Array.String}    iris        input list of IRIs
 * @param     {Number}          minProb     minimum predicate probability to be included as a candidate facet
 * @returns
 */
export default async function generateCandidates( conn, iris, minProb ) {

  // run query for direct facets
  let formatedQuery = queryCandidates( iris, minProb );
  const resp_direct = await conn( formatedQuery );

  // get a list of direct property IRIs
  // exclude those pointing to literals
  const props = resp_direct
    .filter( (row) => !row.dt )
    .map( (row) => { return { type: 'iri', value: row.prop1 }; } );

  // run query for indirect facets
  formatedQuery = queryCandidates_indirect( iris, props, minProb );
  const resp_indirect = await conn( formatedQuery );

  // convert to Facet objects
  return [ ... resp_direct, ... resp_indirect ].map( (entry) => {
    const f = new Facet( [ entry.prop1, entry.prop2 ].filter( (p) => p ) , entry.dt || 'http://www.w3.org/2002/07/owl#Thing' );
    f.setScore( 'probability', entry.probScore );
    return f;
  });

}
