'use strict';
import queryLabels      from    './queries/labels';
import queryLabelsProp  from    './queries/labelsProp';
/**
 * add labels to properties and values for the given facets
 *
 * @param   {Function}        conn          wrapper function to issue queries through
 * @param   {Array.Facet}     facet         the facets to augment
 * @returns
 */
export default async function addLabels( conn, facets ){

  // collect IRIs to resolve
  const iris  = new Set(),
        props = new Set();
  for( const facet of facets ) {

    // properties
    facet.path
      .forEach( (p) => props.add( p.iri ) );

    // values
    if( facet.datatype == 'http://www.w3.org/2002/07/owl#Thing' ) {
      facet.values
        .forEach( (v) => iris.add( v.iri ) );
    }

  }

  // request the labels
  let input   = [ ... iris ].map( (i) => { return { type: 'iri', value: i }; } ),
      query   = queryLabels( input );
  const reqIri  = conn( query );
  input = [ ... props ].map( (i) => { return { type: 'iri', value: i }; } ),
  query = queryLabelsProp( input );
  const reqProp = conn( query );

  // wait for the requests to finish
  await Promise.all([ reqIri, reqProp ]);

  // parse into a map
  let labelMap = {};
  (await reqIri).forEach( (el) => labelMap[ el.iri ] = el.iriLabel );
  (await reqProp).forEach( (el) => labelMap[ el.prop ] = el.iriLabel );

  // attach labels
  for( const facet of facets ) {

    // properties
    facet.path
      .forEach( (p) => p.label = labelMap[ p.iri ] );

    // values
    if( facet.datatype == 'http://www.w3.org/2002/07/owl#Thing' ) {
      facet.values
        .forEach( (v) => v.label = labelMap[ v.iri ] );
    }

  }

}
