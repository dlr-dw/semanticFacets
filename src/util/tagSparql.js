'use strict';
/**
 * will escape the given values inside the SPARQL template given
 *
 *
 * @param     {Array.String}    strings       parts of the template string
 * @param     {...*}            values        values associated within
 * @returns   String                          formated SPARQL query
 */
export default function tagSparql( strings, ...values ) {

  // collect all results in a string array
  const result = [ strings[0] ];
  for( let i=0; i<values.length; i++ ) {

    // format the given value
    result.push( valueToSparql( values[i] ) );

    // ... and the next string
    result.push( strings[i+1] );

  }

  // stitch everything together
  return result.join( '' );

}

/**
 * format a single value into a SPARQL-compatible fashion
 *
 * @param     {*}         value     value to be converted
 * @returns   {String}              String representing the value in SPARQL
 */
function valueToSparql( value ) {

  switch( true ) {

    case (typeof value == 'boolean' ):
      return `"${value}"^^xsd:boolean`;

    case (typeof value == 'string'):
      return JSON.stringify( value );

    case Number.isInteger( value ):
      return `"${value}"^^xsd:integer`;

    case (typeof value == 'number' && !Number.isInteger( value )):
      return `"${value}"^^xsd:double`;

    case (value instanceof Array):
      return value.map( valueToSparql ).join( ' ' );

    case (typeof value == 'object'):

      switch( value.type ) {

        case 'iri':
        case 'url':
        case 'uri':
          return `<${value.value}>`;

        case 'string':
          if( 'lang' in value ) {
            return JSON.stringify( value.value ) + `@${value.lang}`;
          } else {
            return JSON.stringify( value.value );
          }

        case 'raw':
          return value.value;

        default:
          throw new Error( `Unsupported type in object value: "${value.type}"` );
      }

    default:
      throw new Error( `Unsupported value type: "${typeof value}"` );

  }

}