/*
 * public facing API of the library
 */

// polyfill
import 'cross-fetch/polyfill';

import getFacets            from './getFacets';
import ThrottledRequest     from './util/ThrottledRequests';

export {
  getFacets as default,
  getFacets,
  ThrottledRequest,
};
