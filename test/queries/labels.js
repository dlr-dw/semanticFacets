'use strict';

import { assert }     from    'chai';
import conn           from    './../_fixtures/conn';
import query          from    './../../src/queries/labels';
import schema         from    './../_schema/queries/labels.schema';

describe( 'queries/labels [integration]', () => {

  it( 'should be able to collect labls for given IRIs', async () => {

    // prepare input
    const iris = [
      'http://www.wikidata.org/entity/Q8261',     // novel
      'http://www.wikidata.org/entity/Q24925',    // science fiction
    ].map( (el) => {
      return { value: el, type: 'iri' };
    });

    // prepare query and run it
    const formatedQuery = await query( iris ),
          resp          = await conn( formatedQuery );

    // check results
    assert.jsonSchema( resp, schema, 'should adhere to the query output schema' );
    const fakeLabel = resp.filter( (el) => el.iri == el.iriLabel );
    assert.deepEqual( fakeLabel, [], 'should contain no fake labels' );

  });
  
});
