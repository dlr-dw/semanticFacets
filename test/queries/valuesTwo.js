'use strict';

import { assert }     from    'chai';
import testIris       from    './../_fixtures/iris_10';
import conn           from    './../_fixtures/conn';
import query          from    './../../src/queries/valuesTwo';
import schema         from    './../_schema/queries/values.schema';

describe( 'queries/values [integration]', () => {

  it( 'should be able to collect facet values for P135 (movement) ', async () => {

    // prepare input
    const iris = testIris.map( (el) => {
      return { value: el, type: 'iri' };
    });
    const base = { type: 'iri', value: 'http://www.wikidata.org/prop/direct/P50' };
    const path = [ { type: 'iri', value: 'http://www.wikidata.org/prop/direct/P569' } ];

    // prepare query and run it
    const formatedQuery = await query( iris, base, path ),
          resp          = await conn( formatedQuery );

    // check results
    assert.jsonSchema( resp, schema, 'should adhere to the query output schema' );
    const unknown = resp.find( (row) => typeof row.value == 'undefined' );
    assert.isNotNull( unknown, 'should contain an "unknown" element' );
    const hit = resp.find( (row) => row.value == 'http://www.wikidata.org/entity/Q37068' );
    assert.isNotNull( hit, 'should contain an element for "Romanticism"' );

  });
  
});
