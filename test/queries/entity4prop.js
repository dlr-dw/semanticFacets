'use strict';

import { assert }     from    'chai';
import conn           from    './../_fixtures/conn';
import query          from    './../../src/queries/entity4prop';
import schema         from    './../_schema/queries/entity4prop.schema';

describe( 'queries/entity4prop [integration]', () => {

  it( 'should be able to collect facet values for a short list of property-IRIs (5) ', async () => {

    // prepare input
    const props = [
      'http://www.wikidata.org/prop/direct/P31',    // instance of
      'http://www.wikidata.org/prop/direct/P135',   // movement
      'http://www.wikidata.org/prop/direct/P571',   // inception
      'http://www.wikidata.org/prop/direct/P50',    // author
      'http://www.wikidata.org/prop/direct/P495',   // country of origin
    ].map( (el) => {
      return { value: el, type: 'iri' };
    });

    // prepare query and run it
    const formatedQuery = await query( props ),
          resp          = await conn( formatedQuery );

    // check results
    assert.jsonSchema( resp, schema, 'should adhere to the query output schema' );
    assert.isAtLeast( resp.length, props.length, 'should include at least one entry for each requested property' );
    for( const prop of props ) {
      const row = resp.find( (row) => row.prop == prop.value );
      assert.isNotNull( row, `should include and entry for <${prop.value}>` );
    }

  });
  
});
