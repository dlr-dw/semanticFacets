'use strict';

import { assert }     from    'chai';
import testIris       from    './../_fixtures/iris_10';
import testProps      from    './../_fixtures/props_10';
import conn           from    './../_fixtures/conn';
import query          from    './../../src/queries/getCandidates_indirect';
import schema         from    './../_schema/queries/getCandidates_indirect.schema';

describe( 'queries/getCandidates_indirect [integration]', () => {

  it( 'should be able to collect facet values for a short list of IRIs (10) ', async () => {

    // prepare input
    const iris = testIris.map( (el) => {
      return { value: el, type: 'iri' };
    });
    const props = testProps.map( (el) => {
      return { value: el, type: 'iri' };
    });

    // prepare query and run it
    const formatedQuery = await query( iris, props, 0.1 ),
          resp          = await conn( formatedQuery );

    // check results
    assert.jsonSchema( resp, schema, 'should adhere to the query output schema' );
    let prop = resp.find( (row) => (row.prop1 == 'http://www.wikidata.org/prop/direct/P31') 
                                   && (row.prop2 == 'http://www.wikidata.org/prop/direct/P279' ) );
    assert.exists( prop, 'should contain at least one row for "instance of"/"subclass of"' );

  });
  
});
