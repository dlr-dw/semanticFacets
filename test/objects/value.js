'use strict';

import { assert } from 'chai';
import Value      from './../../src/objects/value';

describe( 'objects/values', () => {
  
  it( 'should only accept int values for count', () => {
    
    // string passed
    assert.throws( () => new Value( 'iri', 'label', '0' ), null, null, 'string via constructor' );
    
    // float passed
    assert.throws( () => new Value( 'iri', 'label', 1.34 ), null, null, 'float via constructor' );
    
    // string after init
    const v = new Value( 'iri', 'test', 0 );
    assert.throws( () => v.count = '0', null, null, 'string via setter' );
    
    // float after init
    assert.throws( () => v.count = 1.23, null, null, 'float via setter' );
    
  });


  it( 'should not be able to change IRIs on existing values', () => {
    
    const v = new Value( 'iri', 'test', 0 );
    
    assert.throws( () => v.iri = 'changed' );
    
  });

  
  it( 'should return the value used at construction', () => {

    const v = new Value( 'iri', 'test', 0 );
    
    assert.equal( v.iri,    'iri',    'iri value' );
    assert.equal( v.label,  'test',   'label value' );
    assert.equal( v.count,  0,        'count value' );
    
  });

  
  it( 'should be allowed to change the label', () => {
    
    const v = new Value( 'iri', 'test', 0 );
    
    assert.doesNotThrow( () => v.label = 'changed' );
    
  });

  
  it( 'should be allowed to change the value count', () => {
    
    const v = new Value( 'iri', 'test', 0 );
    
    assert.doesNotThrow( () => v.count = 3 );
    
  });

});