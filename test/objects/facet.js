'use strict';

import { assert } from 'chai';
import Facet      from './../../src/objects/facet';
import Value      from './../../src/objects/value';
import Scores     from './../../src/config/scores';

describe( 'objects/facet', () => {
  
  it( 'should accept a single IRI in the constructor', () => {
    
    assert.doesNotThrow( () => new Facet( 'http://example.org/prop' ) );
    
  });
  
  
  it( 'should always return an array as property path', () => {
   
    // single IRI input
    const f1 = new Facet( 'http://example.org/prop' );
    assert.deepEqual( f1.path.map( (p) => p.iri ), [ 'http://example.org/prop' ], 'single IRI string in constructor' );
    
    // multiple IRIs
    const f2 = new Facet( [ 'http://example.org/prop1', 'http://example.org/prop2' ] );
    assert.deepEqual( f2.path.map( (p) => p.iri ), [ 'http://example.org/prop1', 'http://example.org/prop2' ], 'list of IRIs in constructor' );
    
  });
  
  
  it( 'should return null, if no values have been attached', () => {
    
    const f = new Facet( 'http://example.org/prop' );
    assert.equal( f.values, null );
    
  });

  
  it( 'should accept all scores as defined', () => {

    const f = new Facet( 'http://example.org/prop' );
    
    for( const score of Scores ) {
      for( const alias of score.alias ) {
        assert.doesNotThrow( () => f.setScore( alias, 0 ) );
      }
    }
    
  });

  
  it( 'should not accept invalid scores or score values', () => {

    const f = new Facet( 'http://example.org/prop' );

    // invalid name
    assert.throws( () => f.setScore( 'doesNotExist', 0 ), null, null, 'invalid name' );
    
    // invalid value
    assert.throws( () => f.setScore( Scores[0].alias[0], 3 ), null, null, 'invalid value - too large' );
    assert.throws( () => f.setScore( Scores[0].alias[0], -0.2 ), null, null, 'invalid value - negative' );

  });

  
  it( 'should only allow to retrieve known scores', () => {
    
    const f = new Facet( 'http://example.org/prop' );
    
    assert.throws( () => f.getScore( 'doesNotExist' ), null, null, 'invalid score name' );

    assert.doesNotThrow( () => f.getScore( Scores[0].alias[0] ), null, null, 'valid score name' );

  });
  
  
  it( 'should not allow to change the property path on existing objects', () => {
    
    const f = new Facet( 'http://example.org/prop' );
    
    assert.throws( () => f.path = 'abc' );
    
  });
  
  
  it( 'should allow to add additional values to a facet', () => {

    const f   = new Facet( 'http://example.org/prop' ),
          v1  = new Value( 'iri1', 'label', 0 ),
          v2  = new Value( 'iri2', 'other label', 0 );
    
    assert.doesNotThrow( () => {
      f.addValue( v1 ); 
      f.addValue( v2 );
    });
    
  });
  
  
  it.skip( 'should automatically remove duplicates based on IRI', () => {
    
    const f   = new Facet( 'http://example.org/prop' ),
          v1  = new Value( 'iri1', 'label', 0 ),
          v2  = new Value( 'iri2', 'other label', 0 ),
          v3  = new Value( 'iri1', 'changed label', 0 );
    
    // no duplicates
    f.values = [ v1, v2 ];
    assert.deepEqual( f.values, [ v1, v2 ], 'set values directly - no duplicates' );
    
    // duplicate object
    f.values = [ v1, v1 ];
    assert.deepEqual( f.values, [ v1 ], 'set values directly - duplicate object' );

    // duplicate IRI
    // later entry dominates earlier entry
    f.values = [ v1, v3 ];
    assert.deepEqual( f.values, [ v3 ], 'set values directly - duplicate object' );
    
    // no duplicates
    f.values = [ v1 ];
    f.addValue( v2 );
    assert.deepEqual( f.values, [ v1, v2 ], 'addValue() - no duplicates' );
    
    // duplicate object
    f.values = [ v1 ];
    f.addValue( v1 );
    assert.deepEqual( f.values, [ v1 ], 'addValue() - duplicate object' );

    // duplicate IRI
    // later entry dominates earlier entry
    f.values = [ v1 ];
    f.addValue( v3 );
    assert.deepEqual( f.values, [ v3 ], 'addValue() - duplicate object' );

  });
  
  
  it( 'should allow to set single instance via values property', () => {
    
    const f   = new Facet( 'http://example.org/prop' ),
          v   = new Value( 'iri', 'label', 0 );
    f.values = v;
    
    assert.deepEqual( f.values, [ v ] );
    
  });


});