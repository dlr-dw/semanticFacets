'use strict';
import { assert } from 'chai';
import getAncestors   from './../../../src/scores/semanticSimilarity/getAncestors';

// data import
import dataAncestorsQ5322166 from './../../_fixtures/queryResults/ancestors_Q5322166';
import dataAncestorsQ7184903 from './../../_fixtures/queryResults/ancestors_Q7184903';
import resultQ5322166        from './../../_fixtures/scores/semanticSimilarity/getAncestors_Q5322166';

describe( 'scores/semanticSimilarity/addAncestors', () => {

  it( 'should work for a small sample', async () => {

    // mock the connection
    // letters for nodes; numbers for distance (from root)
    function mockConn( query ){
      if( query.includes( '## query: ancestors_direct' ) ) {
        return [
          {'parent':'AAAA3'},
          {'parent':'ABA2'},
          {'parent':'ABB2'},
        ];
      } else {
        return [
          { middle: 'AA1',    parent: 'A0' },
          { middle: 'AB1',    parent: 'A0' },
          { middle: 'AAA2',   parent: 'AA1' },
          { middle: 'AAAA3',  parent: 'AAA2' },
          { middle: 'ABA2',   parent: 'AB1' },
          { middle: 'ABB2',   parent: 'AB1' },
        ];
      }
    };

    // run code
    const result = await getAncestors( mockConn, 'testProp' );

    // shortcuts
    const exp = {
      'testProp': 0,
      'AAAA3':    1,
      'AAA2':     2,
      'ABA2':     1,
      'ABB2':     1,
      'AA1':      3,
      'AB1':      2,
      'A0':       3,
    };

    // checks
    assert.deepEqual( result, exp, 'should provide the expected mapping from nodes to distances' );

  });


  it( 'should be able to cope with cycles in the poly-hierarchy', async () => {

    // mock the connection
    // derived from ancestors of wd:Q7184903 ('abstract object') @ 2019-04-23
    function mockConn( query ){
      if( query.includes( '## query: ancestors_direct' ) ) {
        return [
          {'parent':'http://www.wikidata.org/entity/Q23958852'},
          {'parent':'http://www.wikidata.org/entity/Q488383'}
        ];
      } else {
        return dataAncestorsQ7184903;        
      }

    }

    // run code
    const result = await getAncestors( mockConn, 'http://www.wikidata.org/entity/Q7184903' );

    // shortcuts
    const exp = {
      'http://www.wikidata.org/entity/Q7184903':  0,
      'http://www.wikidata.org/entity/Q35120':    2,
      'http://www.wikidata.org/entity/Q151885':   3,
      'http://www.wikidata.org/entity/Q488383':   1,
      'http://www.wikidata.org/entity/Q714737':   7,
      'http://www.wikidata.org/entity/Q937228':   6,
      'http://www.wikidata.org/entity/Q1207505':  7,
      'http://www.wikidata.org/entity/Q2145290':  4,
      'http://www.wikidata.org/entity/Q4393498':  5,
      'http://www.wikidata.org/entity/Q5127848':  3,
      'http://www.wikidata.org/entity/Q19478619': 2,
      'http://www.wikidata.org/entity/Q19868531': 3,
      'http://www.wikidata.org/entity/Q23958852': 1,
      'http://www.wikidata.org/entity/Q23960977': 2,
      'http://www.wikidata.org/entity/Q33104279': 4,

    };

    // checks
    assert.deepEqual( result, exp, 'should provide the expected mapping from nodes to distances' );

  });


  it( 'should be independent of the order in query results', async () => {

    // mock the connection
    // derived from ancestors of wd:Q5322166 ('designer') @ 2019-04-26
    function mockConn( query ){
      if( query.includes( '## query: ancestors_direct' ) ) {
        return [
          {'parent':'http://www.wikidata.org/entity/Q28640'},
          {'parent':'http://www.wikidata.org/entity/Q2500638'}
        ];
      } else {
        return dataAncestorsQ5322166.sort( (a,b) => a.parent.localeCompare( b.parent ) );        
      }
    }

    // run code
    const result = await getAncestors( mockConn, 'http://www.wikidata.org/entity/Q5322166' );

    // checks
    assert.deepEqual( result, resultQ5322166, 'should provide the expected mapping from nodes to distances' );

  });
});
