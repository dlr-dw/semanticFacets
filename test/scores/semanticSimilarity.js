'use strict';

import { assert } from 'chai';
import Facet      from './../../src/objects/facet';
import scoring    from './../../src/scores/semanticSimilarity';

// data imports
import dataGetAncestorsQ215627    from './../_fixtures/scores/semanticSimilarity/getAncestors_Q215627';
import dataGetAncestorsQ5322166   from './../_fixtures/scores/semanticSimilarity/getAncestors_Q5322166';

describe( 'scores/semanticSimilarity', () => {


  it( 'should work for facets with similar properties', async () => {

    // inputs
    const f1 = new Facet( 'http://example.org/prop1' ),
          f2 = new Facet( 'http://example.org/prop2' );

    // mock entities and ancestors
    const commonParents = {
      'http://example.org/parent1': 3,
      'http://example.org/parent2': 2 
    };
    f1.path[0].entity = 'http://example.org/entity1';
    f1.path[0].ancestors = { ...commonParents, 'http://example.org/parent3': 1 };
    f2.path[0].entity = 'http://example.org/entity2';
    f2.path[0].ancestors = { ...commonParents, 'http://example.org/parent4': 1 };

    // mock connection function for ancestor queries
    function connMock( query ) {
      switch( true ) {

        // entity queries
        case query.includes( '## query: ancestors_direct' ):
          return [
            { parent: 'http://example.org/parent1' },
          ];

        // similarity query
        case query.includes( '## query: ancestors' ):
          return [];
          
        default: throw new Error( 'Unexpected query called.' );

      }
    }

    // check similarity
    const result = scoring( connMock, [ f1 ], f2 );
    await assert.eventually.isAbove( result, 0, 'score should be above 0.5' );

  });

  
  it( 'should work, if one entity is an ancestor of the other', async () => {

    // inputs
    const f1 = new Facet( 'http://example.org/prop1' ),
          f2 = new Facet( 'http://example.org/prop2' );

    // mock entities and ancestors
    f1.path[0].entity = 'http://www.wikidata.org/entity/Q215627'; /* person */
    f1.path[0].ancestors = dataGetAncestorsQ215627;
    f2.path[0].entity = 'http://www.wikidata.org/entity/Q5322166'; /* designer */
    f2.path[0].ancestors = dataGetAncestorsQ5322166;

    // mock connection function for ancestor queries
    function connMock( query ) {
      throw new Error( 'Unexpected query called.' );
    }

    // init the scoring cache
    await scoring( connMock, [], f1 );

    // check similarity
    const result = scoring( connMock, [ f1 ], f2 );
    await assert.eventually.isAbove( result, 0, 'score should be above 0.5' );

  });


  it( 'should return 0, if input facet has no entity', async () => {

    // inputs
    const f1 = new Facet( 'http://example.org/prop1' );

    // mock connection function
    function connMock( query ) {
      throw Error( 'No queries are needed' );
    }

    // check similarity
    const result = scoring( connMock, [], f1 );
    await assert.eventually.equal( result, 0 );

  });


  it( 'should return 0, if there is no bulk facets', async () => {

    // inputs
    const f1 = new Facet( 'http://example.org/prop1' );

    // mock connection function
    function connMock( query ) {
      throw Error( 'No queries are needed' );
    }

    // check similarity
    const result = scoring( connMock, [], f1 );
    await assert.eventually.equal( result, 0 );

  });


  it( 'should throw, when the connection throws', async () => {

    // inputs
    const f1 = new Facet( 'http://example.org/prop1' ),
          f2 = new Facet( 'http://example.org/prop2' );

    // mock entities and ancestors
    const commonparentThrows = {
      'http://example.org/parentThrow1': 3,
      'http://example.org/parentThrow2': 2 
    };
    f1.path[0].entity = 'http://example.org/entity1';
    f1.path[0].ancestors = { ...commonparentThrows, 'http://example.org/parentThrow3': 1 };
    f2.path[0].entity = 'http://example.org/entity2';
    f2.path[0].ancestors = { ...commonparentThrows, 'http://example.org/parentThrow4': 1 };

    // mock connection function
    function connMock( query ) {
      throw new Error( 'Mock connection error.' );
    }

    // check similarity
    const req = scoring( connMock, [ f1 ], f2 );
    await assert.isRejected( req );

  });

});
