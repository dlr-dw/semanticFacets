'use strict';

import { assert } from 'chai';
import Facet      from './../../src/objects/facet';
import Value      from './../../src/objects/value';
import scoring    from './../../src/scores/valueCardinality';
import Settings   from './../../src/config/valueCardinality';

describe( 'scores/valueCardinality', () => {

  it( 'should calculate the value cardinality such that outOfRange <= average <= optimum', () => {

    const f = new Facet( 'http://example.org/prop' );
    
    // below minimum
    f.values = createValues( Settings.minimum - 1 );
    scoring( f );
    const scoreBelowMin = f.getScore( 'cardinality' );

    // minimum < X < optimum
    f.values = createValues( Settings.minimum + (Settings.optimum - Settings.minimum) / 2 );
    scoring( f );
    const scoreAvg1 = f.getScore( 'cardinality' );
    
    // optimum
    f.values = createValues( Settings.optimum );;
    scoring( f );
    const scoreOpt = f.getScore( 'cardinality' );

    // beyond optimum
    f.values = createValues( 1.2 * Settings.optimum );;
    scoring( f );
    const scoreAvg2 = f.getScore( 'cardinality' );

    // assert the (half-)order of scores
    assert.equal( scoreBelowMin,  0, 'out of range' );
    assert.equal( scoreOpt,       1, 'optimum' );
    assert.isAtMost( scoreAvg1,   scoreOpt, 'score(avgLow) < score(opt)' );
    assert.isAtMost( scoreAvg2,   scoreOpt, 'score(avgHigh) < score(opt)' );

    // assert range in [0,1]
    assert.isAtMost( scoreAvg1, 1, 'score(avgLow) <= 1' );
    assert.isAtLeast( scoreAvg1, 0, '0 <= score(avgLow)' );
    assert.isAtMost( scoreAvg2, 1, 'score(avgHigh) <= 1' );
    assert.isAtLeast( scoreAvg2, 0, '0 <= score(avgHigh)' );

  });

});

/**
 * create the requested number of distinct Value objects
 * makes sure the number is actually an integer
 */
function createValues( number ) {
  number = Math.round( number );
  return (new Array( number ))
            .fill( null )
            .map( (el,ind) => new Value( `http://example.org/entity${ind}`, `Value ${ind}`, 0 ) );
}