'use strict';

import { assert } from 'chai';
import Facet      from './../../src/objects/facet';
import Value      from './../../src/objects/value';
import scoring    from './../../src/scores/valueDispersion';

describe( 'scores/valueDispersion', () => {
  
  it( 'should calculate the value dispersion as defined for the optimal case', () => {

    const f = new Facet( 'http://example.org/prop' );
    
    // ideal score
    f.values = [
      new Value( 'http://example.org/entitiy1', 'Value 1',  5 ),
      new Value( 'http://example.org/entitiy2', 'Value 2',  5 ),
      new Value( 'http://example.org/entitiy3', 'Value 3',  5 ),
    ];
    scoring( f );
    assert.equal( f.getScore( 'dispersion' ), 1, '[ 5, 5, 5 ] - ideal score' );
  });

  
  it( 'should calculate the value dispersion as defined for an average case', () => {

    const f = new Facet( 'http://example.org/prop' );

    // simple example
    f.values = [
      new Value( 'http://example.org/entitiy1', 'Value 1',  3 ),
      new Value( 'http://example.org/entitiy2', 'Value 2',  5 ),
      new Value( 'http://example.org/entitiy3', 'Value 3', 10 ),
    ];
    scoring( f );
    const scoreSimple = f.getScore( 'dispersion' );
    assert.isAtMost(  scoreSimple, 1, 'score(avgLow) <= 1' );
    assert.isAtLeast( scoreSimple, 0, '0 <= score(avgLow)' );

  });

});