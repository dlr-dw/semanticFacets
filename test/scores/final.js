'use strict';
import { assert } from 'chai';
import Facet      from './../../src/objects/facet';
import Scores     from './../../src/config/scores';
import finalScore from './../../src/scores/final';

describe( 'scores/final', () => {

  it( 'should be 1, if all parts are 1', () => {
    
    // input
    const facet = new Facet( 'http://example.org/prop', 'Property 1', 10 );
    for( const score of Scores ) {
      if( !score.final ) {
        facet.setScore( score.alias[ 0 ], 1 );
      }
    }

    // calc result
    finalScore( facet );

    // check
    assert.equal( facet.getScore( 'final' ), 1 );

  });


  it( 'should be 0, if all parts are 0', () => {
    
    // input
    const facet = new Facet( 'http://example.org/prop', 'Property 1', 10 );
    for( const score of Scores ) {
      if( !score.final ) {
        facet.setScore( score.alias[ 0 ], 0 );
      }
    }

    // calc result
    finalScore( facet );

    // check
    assert.equal( facet.getScore( 'final' ), 0 );

  });

});
