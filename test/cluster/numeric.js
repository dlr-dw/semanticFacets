'use strict';

import { assert }   from 'chai';
import cluster      from './../../src/cluster/numeric';
import Value        from './../../src/objects/value';
import RangeValue   from './../../src/objects/rangeValueNumeric';

describe( 'cluster/numeric', () => {

  createTest({
    name:         'should partition a set of values including "unknown"', 
    elementCount: 9,
    bucketCount:  3, 
    hasUnknown:   true,
  });

  createTest({
    name:         'should partition a set of values excluding "unknown"', 
    elementCount: 9,
    bucketCount:  3, 
    hasUnknown:   false,
  });

  createTest({
    name:         'should parition a set of values including "unknown" whose number of elements is no multiple of bucketCount', 
    elementCount: 9,
    bucketCount:  4, 
    hasUnknown:   true,
  });

  createTest({
    name:         'should parition a set of values excluding "unknown" whose number of elements is no multiple of bucketCount', 
    elementCount: 9,
    bucketCount:  4, 
    hasUnknown:   false,
  });

  /**
   * create our testcases automatically
   *
   * @param   {Object}      param                   parameter list for test
   * @param   {String}      param.name              name of the testcase
   * @param   {Number}      param.elementCount      number of elements in the input set
   * @param   {Number}      param.bucketCount       number of buckets
   * @param   {Boolean}     param.hasUnknown        is there an unknown value included?
   */
  function createTest( param ) {

    it( param.name, () => {

      // create input array
      const input = (new Array( param.hasUnknown ? param.elementCount - 1 : param.elementCount ) )
                    .fill( true )
                    .map( (el,ind) => new Value( '' + ind, '' + ind, 1 ) );

      // add unknown, if requested 
      const unknown = Object.freeze( new Value( null, 'unknown', 1 ) );
      if( param.hasUnknown ){
        input.push( unknown );
      }

      // run clustering
      const clustered = cluster( input, param.bucketCount );

      // shortcut
      const resUnknown = clustered.find( (c) => c.iri === null );

      // check results
      assert.equal( clustered.length, param.bucketCount, `should return ${param.bucketCount} objects` );
      assert.isOk( clustered.every( (c) => c instanceof Value ), 'should return only Value objects' );
      if( !param.hasUnknown ) {
        assert.isOk( clustered.every( (c) => c instanceof RangeValue ), 'should return only RangeValueNumeric objects' );      
      } else {
        assert.equal( clustered.filter( (c) => !(c instanceof RangeValue) ).length, 1,                      'should return 1 Value object ("unknown")' );
        assert.equal( clustered.filter( (c) => c instanceof RangeValue ).length,    param.bucketCount - 1,  `should return ${param.bucketCount-1} RangeValueNumeric objects` );
      }
      const clusterSize = Math.trunc( input.length / param.bucketCount );
      for( const el of clustered ) {
        if( el instanceof RangeValue ) {
          assert.closeTo( el.max - el.min + 1, 1, clusterSize, 'each object almost the same range' );
        } else {
          assert.deepEqual( el, unknown, 'should preserve the unknown object unchanged' );
        }
      }

    });

  }

});
