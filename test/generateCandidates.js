'use strict';

import { assert }         from 'chai';
import conn               from './_fixtures/conn';
import testIris           from './_fixtures/iris_10';
import generateCandidates from './../src/generateCandidates';
import Facet              from './../src/objects/facet';


describe( 'generateCandidates [integration]', () => {

  it( 'should generate a list of candidates for a short list of IRIs (10)', async function() {

    // increase the timeout
    this.timeout( 10 * 1000 );

    // prepare input
    const iris = testIris.map( (el) => {
            return { value: el, type: 'iri' };
          }),
          minProp = 0.1;

    // get candidates
    const candidates = await generateCandidates( conn, iris, minProp );

    // checks
    assert.isArray( candidates, 'should return an array' );
    assert.isTrue( candidates.every( (c) => c instanceof Facet ), 'each entry should return an array of Facet' );
    assert.isTrue( candidates.every( (c) => c.getScore( 'probability' ) >= minProp ), `each entry should have a probability score greater than the one requested (${minProp})` );
    assert.isTrue( candidates.every( (c) => c.datatype ), 'each entry should have a datatype attached' );
    assert.isTrue( candidates.every( (c) => c.path ), 'each entry should have a property path attached' );
    assert.isTrue( candidates.some( (c) => c.path.length == 1 ), 'should include some direct properties as candidates' );
    assert.isTrue( candidates.some( (c) => c.path.length == 2 ), 'should include some indirect properties as candidates' );

  });

});
