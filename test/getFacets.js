'use strict';
import { assert }         from 'chai';
import conn               from './_fixtures/conn';
import testIris           from './_fixtures/iris_10';
import getFacets          from './../src/getFacets';
import Facet              from './../src/objects/facet';

describe( 'index [integration]', () => {

  it( 'should work on a small set of IRIs (10)', async function() {

    // increase the timeout
    this.timeout( 30 * 1000 );

    // input
    const facetCount = 5;

    // retrieve facets
    const facets = await getFacets( conn, testIris, facetCount );

    // checks
    assert.isArray( facets, 'should return an array' );
    assert.equal( facets.length, facetCount, 'should return as many facets as requested' );
    assert.isTrue( facets.every( (f) => f instanceof Facet ), 'should return an array of Facet' );
    const finalScores = facets.map( (facet) => facet.getScore( 'final' ) );
    assert.isTrue( finalScores.every( (el,i,arr) => (i == 0) || (el <= arr[i-1]) ), 'should return array in descending order wrt the final score' );

  });

  
  it( 'should throw on invalid inputs', () => {

    assert.isRejected( getFacets( conn, 'abc', 3 ),     null, null, 'should throw on non-array inputs' );
    assert.isRejected( getFacets( conn, [ 1, 2 ], 3 ),  null, null, 'should throw on arrays not containing strings' );

  });

});
