'use strict';

import lint from 'mocha-eslint';
import Path from 'path';

const paths = [
  Path.join( __dirname, '..', 'src' ),
];

const options = {
  formatter:    'compact',
  alwaysWarn:   false,
  timeout:      5000,
  slow:         1000,
  strict:       true, 
  contextName:  '.eslint',
};

lint( paths, options );