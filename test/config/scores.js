'use strict';

import { assert } from 'chai';
import Scores     from './../../src/config/scores';

describe( 'config/scores', () => {
  
  it( 'should have all weights add up to 1', () =>{

    const totalWeight = Scores.reduce( (acc,el) => acc + el.weight, 0 );
    assert.equal( totalWeight, 1 );

  })
  
  
  it( 'should only contain one score marked "final"', () => {
    
    const final = Scores.filter( (el) => el.final );
    assert.equal( final.length, 1 );
    
  })
  
});
