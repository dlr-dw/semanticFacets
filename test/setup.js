'use strict'
/**
 * setup global hooks and general test environment
 */

// chai plugins
import chai from 'chai';

import chai_as_promised from 'chai-as-promised';
chai.use( chai_as_promised );

import chai_json_schema from 'chai-json-schema';
chai.use( chai_json_schema );

// import index.js, so all files appear in coverage report
import './../src/index.js';
