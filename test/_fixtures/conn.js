'use strict';
import Connection from './../../src/util/ThrottledRequests';
import Config     from './../../src/config/config';

// @TODO polyfill, might not be needed forever
import 'cross-fetch/polyfill';

/**
 * provide a common connection to Wikidata for integration tests
 *
 * so we do not flood Wikidata with our requests
 */
export default Connection( Config.endpoint );