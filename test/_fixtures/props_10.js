'use strict';
/**
 * example set of property IRIs for testing
 *
 * - domain is written novels
 * - matches the IRIs of iris_10.js
 */
export default [
  'http://www.wikidata.org/prop/direct/P4969',
  'http://www.wikidata.org/prop/direct/P6216',
  'http://www.wikidata.org/prop/direct/P31',
  'http://www.wikidata.org/prop/direct/P50',
  'http://www.wikidata.org/prop/direct/P123',
  'http://www.wikidata.org/prop/direct/P135',
  'http://www.wikidata.org/prop/direct/P136',
  'http://www.wikidata.org/prop/direct/P155',
  'http://www.wikidata.org/prop/direct/P156',
  'http://www.wikidata.org/prop/direct/P166',
  'http://www.wikidata.org/prop/direct/P179',
  'http://www.wikidata.org/prop/direct/P180',
  'http://www.wikidata.org/prop/direct/P407',
  'http://www.wikidata.org/prop/direct/P495',
  'http://www.wikidata.org/prop/direct/P674',
  'http://www.wikidata.org/prop/direct/P747',
  'http://www.wikidata.org/prop/direct/P840',
  'http://www.wikidata.org/prop/direct/P921',
  'http://www.wikidata.org/prop/direct/P953',
  'http://www.wikidata.org/prop/direct/P1411',
];
