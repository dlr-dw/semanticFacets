/*
 * result of ancestor query for wd:Q7184903 ('abstract object') @ 2019-04-23
 */
export default [
  {middle: 'http://www.wikidata.org/entity/Q151885',   'parent':'http://www.wikidata.org/entity/Q2145290'},
  {middle: 'http://www.wikidata.org/entity/Q151885',   'parent':'http://www.wikidata.org/entity/Q7184903'},
  {middle: 'http://www.wikidata.org/entity/Q488383',   'parent':'http://www.wikidata.org/entity/Q35120'},
  {middle: 'http://www.wikidata.org/entity/Q714737',   'parent':'http://www.wikidata.org/entity/Q151885'},
  {middle: 'http://www.wikidata.org/entity/Q937228',   'parent':'http://www.wikidata.org/entity/Q714737'},
  {middle: 'http://www.wikidata.org/entity/Q937228',   'parent':'http://www.wikidata.org/entity/Q1207505'},
  {middle: 'http://www.wikidata.org/entity/Q1207505',  'parent':'http://www.wikidata.org/entity/Q35120'},
  {middle: 'http://www.wikidata.org/entity/Q2145290',  'parent':'http://www.wikidata.org/entity/Q4393498'},
  {middle: 'http://www.wikidata.org/entity/Q4393498',  'parent':'http://www.wikidata.org/entity/Q937228'},
  {middle: 'http://www.wikidata.org/entity/Q5127848',  'parent':'http://www.wikidata.org/entity/Q7184903'},
  {middle: 'http://www.wikidata.org/entity/Q7184903',  'parent':'http://www.wikidata.org/entity/Q488383'},
  {middle: 'http://www.wikidata.org/entity/Q19478619', 'parent':'http://www.wikidata.org/entity/Q5127848'},
  {middle: 'http://www.wikidata.org/entity/Q19868531', 'parent':'http://www.wikidata.org/entity/Q151885'},
  {middle: 'http://www.wikidata.org/entity/Q23958852', 'parent':'http://www.wikidata.org/entity/Q19478619'},
  {middle: 'http://www.wikidata.org/entity/Q23958852', 'parent':'http://www.wikidata.org/entity/Q23960977'},
  {middle: 'http://www.wikidata.org/entity/Q23960977', 'parent':'http://www.wikidata.org/entity/Q35120'},
  {middle: 'http://www.wikidata.org/entity/Q33104279', 'parent':'http://www.wikidata.org/entity/Q151885'},
  {middle: 'http://www.wikidata.org/entity/Q35120',    'parent':'http://www.wikidata.org/entity/Q23958852'},
  {middle: 'http://www.wikidata.org/entity/Q714737',   'parent':'http://www.wikidata.org/entity/Q33104279'},
  {middle: 'http://www.wikidata.org/entity/Q1207505',  'parent':'http://www.wikidata.org/entity/Q33104279'},
  {middle: 'http://www.wikidata.org/entity/Q2145290',  'parent':'http://www.wikidata.org/entity/Q151885'},
  {middle: 'http://www.wikidata.org/entity/Q5127848',  'parent':'http://www.wikidata.org/entity/Q19478619'},
  {middle: 'http://www.wikidata.org/entity/Q5127848',  'parent':'http://www.wikidata.org/entity/Q33104279'},
  {middle: 'http://www.wikidata.org/entity/Q7184903',  'parent':'http://www.wikidata.org/entity/Q23958852'},
  {middle: 'http://www.wikidata.org/entity/Q19478619', 'parent':'http://www.wikidata.org/entity/Q151885'},
  {middle: 'http://www.wikidata.org/entity/Q19478619', 'parent':'http://www.wikidata.org/entity/Q19868531'},
  {middle: 'http://www.wikidata.org/entity/Q19478619', 'parent':'http://www.wikidata.org/entity/Q23958852'},
  {middle: 'http://www.wikidata.org/entity/Q23958852', 'parent':'http://www.wikidata.org/entity/Q23958852'},
  {middle: 'http://www.wikidata.org/entity/Q23960977', 'parent':'http://www.wikidata.org/entity/Q23958852'},
];