'use strict';
/**
 * example set of IRIs for testing
 *
 * - domain is written novels
 */
export default [
  'http://www.wikidata.org/entity/Q150827', 
  'http://www.wikidata.org/entity/Q182961', 
  'http://www.wikidata.org/entity/Q212340', 
  'http://www.wikidata.org/entity/Q212898', 
  'http://www.wikidata.org/entity/Q235795', 
  'http://www.wikidata.org/entity/Q264182', 
  'http://www.wikidata.org/entity/Q287838', 
  'http://www.wikidata.org/entity/Q313129', 
  'http://www.wikidata.org/entity/Q318891', 
  'http://www.wikidata.org/entity/Q373479',
];
