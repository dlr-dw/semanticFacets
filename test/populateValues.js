'use strict';

import { assert }         from 'chai';
import conn               from './_fixtures/conn';
import testIris           from './_fixtures/iris_10';
import populate           from './../src/populateValues';
import Facet              from './../src/objects/facet';
import Value              from './../src/objects/value';
import RangeValueDate     from './../src/objects/rangeValueDate';
import RangeValueNumeric  from './../src/objects/rangeValueNumeric';


describe( 'populateValues [integration]', () => {
  
  createTest({
    name:       'should work for ObjectProperties',
    prop:       'http://www.wikidata.org/prop/direct/P136',   // genre
    type:       'http://www.w3.org/2002/07/owl#Thing',
    mainClass:  Value,
    hasUnknown: true,
  });
  
  createTest({
    name:       'should work for DataProperties (date)',
    prop:       'http://www.wikidata.org/prop/direct/P577',   // publication date
    type:       'http://www.w3.org/2001/XMLSchema#dateTime',
    mainClass:  RangeValueDate,
    hasUnknown: true,
  });
  
  createTest({
    name:       'should work for DataProperties (numeric)',
    prop:       [
      'http://www.wikidata.org/prop/direct/P495',   // country of origin
      'http://www.wikidata.org/prop/direct/P1081',  // Human Development Index
    ],
    type:       'http://www.w3.org/2001/XMLSchema#decimal',
    mainClass:  RangeValueNumeric,
    hasUnknown: true,
  });


  it( 'should work, even if there are no values returned (values are blank nodes)', async () => {

      // prepare input
      const iris = testIris.map( (el) => {
        return { value: 'http://www.wikidata.org/entity/Q2870', type: 'iri' };
      });
      const facet = new Facet([
        'http://www.wikidata.org/prop/direct/P495',   // country of origin
        'http://www.wikidata.org/prop/direct/P2997'   // age of majority
      ], 'http://www.w3.org/2002/07/owl#Thing' );

      // execute
      await populate( conn, iris, [ facet ] );

      // check
      assert.isArray( facet.values, 'should augment the facet with an array for values' );
      assert.isEmpty( facet.values, 'should augment the facet with an empty array' );

  });


  /**
   * create our testcases automatically
   * 
   * @param   {Object}      param                   parameter list for test
   * @param   {String}      param.name              name of the testcase
   * @param   {String}      param.prop              IRI of the property
   * @param   {String}      param.type              datatype of the property
   * @param   {Number}      param.mainClass         main class of the resulting Values-objects
   * @param   {Boolean}     param.hasUnknown        is there an unknown value included?
   */
  function createTest( param ) {

    it( param.name, async () => {

      // prepare input
      const iris = testIris.map( (el) => {
        return { value: el, type: 'iri' };
      });
      const facet = new Facet( param.prop, param.type );

      // execute
      await populate( conn, iris, [ facet ] );

      // check
      assert.isArray(    facet.values, 'should augment the facet with an array for values' );
      assert.isNotEmpty( facet.values, 'should augment the facet with a non-empty array' );
      if( param.hasUnknown ) {
        const unknown     = facet.values.find(   (v) => v.iri === null ),
              noUnknown   = facet.values.filter( (v) => v !== unknown );
        assert.isTrue(      noUnknown.every( (v) => v instanceof param.mainClass ), `should augment with an array of ${param.mainClass.name} objects` );
        assert.isNotEmpty(  noUnknown, 'should have at least one non-"unknown" value in the result' );
      } else {
        assert.isTrue( facet.values.every( (v) => v instanceof param.mainClass ), `should augment with an array of ${param.mainClass.name} objects` );
      }

    });

  }

});
