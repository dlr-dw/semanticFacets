[![npm](https://img.shields.io/npm/v/semanticfacets.svg)](https://www.npmjs.com/package/semanticfacets)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.2784142.svg)](https://doi.org/10.5281/zenodo.2784142)


# Semantic Facets

A library for automatic facet generation over SPARQL-endpoints.

The provided code currently just supports the SPARQL endpoint of [Wikidata](http://wikidata.org/).
However, the library can be adapted to any knowledge graph.

## Install

`semanticFacets` is available on [npm](https://npmjs.org).

```
npm install semanticfacets --save
```

## Tests

To run the test first download the repository and install the dependencies. 
Then run `npm test`.

```
npm install
npm test
```

Note that some tests will connect to the Wikidata SPARQL endpoint.

## Usage

```javascript
import { getFacets, ThrottledRequest } from 'semanticfacets';

// get the request from somewhere
const iris = [ /* some Wikidata resource IRIs */ ];

// default endpoint is set to Wikidata
const connection = ThrottledRequest();

const facets = await getFacets( connection, iris, 5 );

// display facets
// ...
```

## API

### `getFacets( connection, iris, facetCount )`

Main entry point to retrieve the facets.

* `connection( query, options )` ... a callback to run SPARQL queries
  * `query` ... a string containing the query itself
  * returns a flattened object containing the query results
* `iris` ... an array of resource IRIs given as string
* `facetCount` ... the number of facets to retrieve
* returns a `Promise` to an array of `Facet`-objects

### `ThrottledRequest( endpoint, maxRequests )`

Factory to create a connection callback for `getFacets()`.

* `endpoint` ... a string containing the URL to a SPARQL endpoint; defaults to the Wikidata endpoint
* `maxRequests` ... maximum number of parallel requests; defaults to 5
* returns a connection callback function for `getFacets()`

### Results

#### `Facet`

Wrapper for a single facet

*Properties*
* `datatype` ... the datatype of this facet's facet values given as an IRI
* `path`      ... the property path for this facet; array of `Property`
* `values`    ... the facet values for this facet; array of `Value`

#### `Property`

Wrapper for RDF-properties

*Properties*
* `iri`        ... the IRI of this property
* `entity`    ... the associated entity's IRI for this property
* `label`     ... the label for this property

#### `Value`

Wrapper for facet values

*Properties*
* `iri`       ... the IRI representing this value or `null` for quantitative facets
* `label`     ... the label for this value
* `count`     ... the value size, i.e. number of input resources connected this value

## Benchmarking

In the development we used a bunch of scripts helping us to understand the facet generation process and measure its performance.
All scripts are included in the repository under `/perf/` and can be run using `npm run [name]`.
To configure the respective test, adapt the configuration object at the top of the respective script.

### `benchmark`

Benchmarks the overall facet generation process.

### `countCandidates`

Attempts to retrieve the number of candidates from the SPARQL endpoint for varying path lengths.

### `testSemanticSimilarity`

Runs the semantic similarity for a selected subset of entities.

### `trackQueries`

Measures the number of executions and their respective runtime for all SPARQL queries.
