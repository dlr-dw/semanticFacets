/**
 * spy upon the endpoint connection between facet generation and SPARQL endpoint
 * to collect some statistics about number of calls and timings
 */

// includes
import Time                   from 'microtime';
import Mathjs                 from 'mathjs';
import createConnectionPool   from '../../src/util/ThrottledRequests';
import 'console.table';

// settings
const cfg = {
  cutoff: 5,      // percentage of top-/bottom-values to remove for "middle" statistics
};

export default class SpyConnection {
  
  constructor( endpoint ) {

    // we need the actual connection
    this._realConn = createConnectionPool( endpoint );

    // we track some statistics
    this._queryCalls    = {};
    this._queryTimings  = {};
    this._rowsReturned  = {};

  }

  
  /**
   * get a ThrottledRequest compatible query function
   * insert our data collections in between
   */
  getConnection(){
    return async ( queryString ) => {

      // extract type of query
      const match = /## query: ([^\n\r]*)/m.exec( queryString ),
            query = match[1];
      if( !query ) {
        throw new Error( 'Could not extract query type!' );
      }

      // count the call
      this._queryCalls[ query ] = this._queryCalls[ query ] ? this._queryCalls[ query ] + 1 : 1;
      this._queryTimings[ query ] = this._queryTimings[ query ] || [];
      this._rowsReturned[ query ] = this._rowsReturned[ query ] || [];

      // run the query and track its execution time
      const timings = {};
      const res = await this._realConn( queryString, {
        before: () => timings.start = Time.now(),
        after:  (res) => {
          timings.end = Time.now();
          this._rowsReturned[ query ].push( res instanceof Array ? res.length : 1 );
        }
      });

      // store the execution time
      this._queryTimings[ query ].push( timings.end - timings.start );

      // relay the result
      return res;

    }
  }

  /**
   * print a report of the collected statistics
   */
  printReport() {
    
    // reusable object to collect the stats
    const stats = [];

    // get a lexicographically sorted list of queries encountered 
    const queries = Object.keys( this._queryCalls ).sort( (a,b) => a.localeCompare( b ) );

    // print the number of calls
    stats.length = 0;
    for( const query of queries ) {
      stats.push({
        query,
        calls: this._queryCalls[ query ],
      });
    }
    console.table( '\nCalls', stats );

    // print execution timings
    stats.length = 0;
    for( const query of queries ) {

      // get middle set of values
      this._queryTimings[ query ].sort();
      const cut = Math.trunc( this._queryTimings[ query ].length * (cfg.cutoff / 100) ),
            start = 0 + cut,
            end = this._queryTimings[ query ].length - cut;
      const mid = this._queryTimings[ query ].slice( start, end );

      // collect the stats
      stats.push({
        query,

        avg:        Math.round( Mathjs.mean( this._queryTimings[ query ] ) ),
        median:     this._queryTimings[ query ][ Math.trunc( this._queryTimings[ query ].length / 2 ) ],
        std:        Math.round( Mathjs.std( this._queryTimings[ query ] ) ),

        avgMid:     Math.round( Mathjs.mean( mid ) ),
        medianMid:  mid[ Math.trunc( mid.length / 2 ) ],
        stdMid:     Math.round( Mathjs.std( mid ) ),

        min:        Mathjs.min( this._queryTimings[ query ] ),
        max:        Mathjs.max( this._queryTimings[ query ] ),
        spent:      this._queryTimings[ query ].reduce( (sum,cur) => sum + cur, 0 ),
      });

    }
    console.table( `\nTimings (excluding wait-time in queue)\nMid is middle ${100 - 2*cfg.cutoff}%`, stats );

    // print rows returned
    stats.length = 0;
    for( const query of queries ) {

      // get middle set of values
      this._rowsReturned[ query ].sort();

      // collect the stats
      stats.push({
        query,

        overall:    Mathjs.sum( this._rowsReturned[ query ] ),
        min:        Mathjs.min( this._rowsReturned[ query ] ),
        max:        Mathjs.max( this._rowsReturned[ query ] ),

        avg:        Math.round( Mathjs.mean( this._rowsReturned[ query ] ) ),
        median:     this._rowsReturned[ query ][ Math.trunc( this._rowsReturned[ query ].length / 2 ) ],
        std:        Math.round( Mathjs.std( this._rowsReturned[ query ] ) ),

      });

    }
    console.table( `\nRows returned`, stats );

  }

}
