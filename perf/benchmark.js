/*
 * measure execution times spent on facet generation for different value sizes
 *
 * note that Wikidata caches query results for 5mins
 * (see Malyshev et al., Getting the Most Out of Wikidata: Semantic Technology Usage in Wikipedia’s Knowledge Graph)
 * we need to account for that in the benchmarking
 * - no warmup run
 * - time between iterations > 5mins 
 */

// @TODO polyfill, might not be needed forever
import 'cross-fetch/polyfill';

// includes
import CTable                 from 'console.table';
import Mathjs                 from 'mathjs';
import Time                   from 'microtime';
import Winston                from 'winston';
import Path                   from 'path';
import Fs                     from 'fs';
import getFacets              from '../src/getFacets';
import Config                 from '../src/config/config';
import createConnectionPool   from '../src/util/ThrottledRequests';
import IriPool                from '../test/_fixtures/iris_novels';

(async function(){

  // define tests
  const cfg = {
      iterations:  10,                // iterations per test
      cutoff:      10,                // percentage of top-/bottom-results to ignore

      pauseIteration: 10 * 60 * 1000,     // pause between iterations (needs to be > 5min for valid results)
      pauseError: 1 * 60 * 1000,     // delay before retry upon error
      pauseTest:      10 * 1000,     // delay between tests   
      
      start: [],    // start of each iteration
      end:   [],    // end of each iteration
      
      benchmarks: [{
        name:      'minimal sample (1 sample IRI)',
        iriNumber: 1,
      }],
  };
  for( let i=100; i<4000; i += 100 ) {
    cfg.benchmarks.push({
      name:       `extended sample (${i} sample IRIs)`,
      iriNumber:  i,
    });
  }

  // setup logging
  const logger = Winston.createLogger({
    transports: [
      new Winston.transports.Console({
        format: Winston.format.combine(
          Winston.format.align(),
          Winston.format.timestamp(),
          Winston.format.printf( (info) => `${info.timestamp} ${info.message}` ),
        ),
      }),
    ],
  });

  // initialize the SPARQL-connection
  const conn = createConnectionPool( Config.endpoint );

  for( let iteration=0; iteration<cfg.iterations; iteration++ ) {
    
    // delay all but the first iteration
    if( iteration > 0 ) {
      await delay( cfg.pauseIteration );
    }

    logger.info( `running iteration ${iteration+1}` );
    cfg.start.push( Time.now() );

    // run benchmarks
    for( const test of cfg.benchmarks ) {

      logger.info( `running ${test.name}` );

      // get the IRI sample
      if( test.iriNumber > IriPool.length ) {
        logger.error( `   insufficient samples available; requested ${test.iriNumber} samples, while pool only has ${IriPool.length}` );
        continue;
      }
      const iris = IriPool.slice( 0, test.iriNumber );

      // measure other calls
      test.measurements = test.measurements || { total: [] };
      try {

        const start = Time.now();
        await getFacets( conn, iris, 5, (ev) => {
          test.measurements[ ev ] = test.measurements[ ev ] || [];
          test.measurements[ ev ].push( Time.now() );
        }); 
        const end = Time.now();

        test.measurements.total.push( end-start );
        logger.info( `   ... done ${end-start}µs` );

      } catch( e ) {

        test.errors = test.errors ? test.errors + 1 : 1;
        logger.error( `   received error: ${e.message}` );
        console.log( e )
        await delay( cfg.pauseError );

      }

      await delay( cfg.pauseTest );

    }
    
    logger.info( `finished iteration ${iteration+1}` );
    cfg.end.push( Time.now() );

  }

  // print reports
  const stats = [];
  for( const entry of cfg.benchmarks ){

    // get middle set of values
    Object.keys( entry.measurements ).forEach( (key) => entry.measurements[ key ].sort() );
    const all = entry.measurements.total;
    const cut = Math.trunc( all.length * (cfg.cutoff / 100) ),
          start = 0 + cut,
          end = all.length - cut;
    const mid = all.slice( start, end );

    // collect the stats
    stats.push({
      name:       entry.name,
      samples:    entry.iriNumber,
      errors:     entry.errors || 0,

      avg:        Math.round( Mathjs.mean( all ) ),
      median:     all[ Math.trunc( all.length / 2 ) ],
      std:        Math.round( Mathjs.std( all ) ),

      avgMid:     Math.round( Mathjs.mean( mid ) ),
      medianMid:  mid[ Math.trunc( mid.length / 2 ) ],
      stdMid:     Math.round( Mathjs.std( mid ) ),

      min:        Mathjs.min( all ),
      max:        Mathjs.max( all ),

      all,
      mid,
    });

  }
  const reducedStats = stats.map( (row) => {
    return { ... row, all: '[see benchmarks.json]', mid: '[see benchmarks.json]' };
  });
  console.table( `\nTimings\nMid is middle ${100 - 2*cfg.cutoff}%`, reducedStats );
  
  // write to file
  const jsonPathFull = Path.join( __dirname, '..', `${timestamp()} benchmarks.details.json` );
  Fs.writeFile( jsonPathFull, JSON.stringify( cfg ), (err) => {
   if( err ) {
     logger.error( err.message );
   } else {
     logger.info( `result summaries written to ${jsonPathFull}` );
   }
  });
  const jsonPathSummary = Path.join( __dirname, '..', `${timestamp()} benchmarks.summary.json` );
  Fs.writeFile( jsonPathSummary, JSON.stringify( stats ), (err) => {
   if( err ) {
     logger.error( err.message );
   } else {
     logger.info( `result summaries written to ${jsonPathSummary}` );
   }
  });
  const textPath = Path.join( __dirname, '..', `${timestamp()} benchmarks.summary.txt` );
  Fs.writeFile( textPath, CTable.getTable( reducedStats ), (err) => {
    if( err ) {
      logger.error( err.message );
    } else {
      logger.info( `result summaries written to ${textPath}` );
    }
   });

})().catch( (e) => console.error( e ) );


function delay( ms ) {
  return new Promise( (resolve) => setTimeout( resolve, ms ) );
}

function timestamp(){
  const now = new Date();
  return now.toISOString().replace( /[:\.]/gi, '-' );
}
