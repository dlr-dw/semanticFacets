/*
 * count the number of candidates dependent on input IRIs 
 */

// @TODO polyfill, might not be needed forever
import 'cross-fetch/polyfill';

// includes
import CTable                 from 'console.table';
import Winston                from 'winston';
import Path                   from 'path';
import Fs                     from 'fs';
import Config                 from '../src/config/config';
import createConnectionPool   from '../src/util/ThrottledRequests';
import IriPool                from '../test/_fixtures/iris_novels';
import queryDirect            from '../src/queries/getCandidates';
import queryIndirectL1        from '../src/queries/getCandidates_indirect';
import queryIndirectL2        from '../src/queries/getCandidates_indirect2';

(async function(){

  // define tests
  const cfg = {

      pause: 10 * 1000,    // time between queries

      min:   100,      // minimum number of IRIs
      max:  4000,      // maximum number of IRIs
      step: 100,       // stepping between tests
      
      intermediateHops: 2,  // maximum considered path length (inclusive) given as number of intermediate hops excluding start-IRI and facet value

      chunkSizeProp: 20,   // maximum number of property paths per query

      maxRequests:     4,   // number of maximum parallel requests

      hashProps: [ 'level', 'prop1', 'prop2', 'prop3' ],   // identifying properties of query results

      tmpFile: Path.join( __dirname, 'countCandidates_intermediate.json' ),     // file to store intermediate results
      resFile: Path.join( __dirname, 'countCandidates_result.json' ),           // file to store final results

      compressProps: [ 'prop' ],                                // prefixes for properties to compress
      compressPrefix: {                                         // IRI prefixes to remove in compression
        'http://www.wikidata.org/prop/direct/': 'wdt:',
        'http://www.wikidata.org/entity/':      'wd:',
      },   

  };

  // setup logging
  const logger = Winston.createLogger({
    transports: [
      new Winston.transports.Console({
        format: Winston.format.combine(
          Winston.format.align(),
          Winston.format.timestamp(),
          Winston.format.printf( (info) => `${info.timestamp} ${info.message}` ),
        ),
      }),
    ],
  });

  // initialize the SPARQL-connection
  const conn = createConnectionPool( Config.endpoint, cfg.maxRequests );

  // load work packages in chunks or create them anew
  let chunks;
  try {
    
    // load from file
    const raw = await Fs.promises.readFile( cfg.tmpFile, 'utf8' );
    chunks = JSON.parse( raw );
    logger.info( `read inputs from file` );
    
  } catch(e) {

    // prepare iris for direct usage in queries
    const IriPoolEscaped = IriPool.map( (iri) => Object.keys( cfg.compressPrefix ).reduce( (iri, prefix) => iri.replace( prefix, cfg.compressPrefix[ prefix ] ) , iri ) )
                                  .map( (iri) => ({ type: 'raw', value: iri }) )
                                  .slice( 0, cfg.max );

    // chunked IRI array
    chunks = [];
    for( let i=0; i<IriPoolEscaped.length; i+=cfg.step ){
      chunks.push({
        id:         (i / cfg.step) + 1,
        iriNumber:  i + cfg.step,
        iris:       IriPoolEscaped.slice( i, i + cfg.step ),
      });
    }

    // log
    logger.info( `created inputs from scratch` );

  }

  // process all chunks
  let needPause = false;
  for( const chunk of chunks ) {

    // possibly delay execution
    if( needPause ) {
      logger.info( `pause ${cfg.pause / 1000}s` );
      await delay( cfg.pause );
    } else {
      needPause = true;
    }

    // log
    logger.info( `starting queries for chunk #${chunk.id} of ${chunks.length}` );

    // check, if already processed
    if( containsResults( chunk, cfg.intermediateHops ) ) {
      logger.info( `   all data present; skipped` );
      needPause = false;
      continue;
    }

    // run the queries
    try{
      chunk.queryResult = await processChunk( chunk );
    } catch( e ) {
      console.log( e );
      logger.info( `   error: ${e.message}` );
      continue;
    }

    // store intermediate results in file
    await Fs.promises.writeFile( cfg.tmpFile, JSON.stringify( chunks ) );

  }


  // create accummulated results
  const properties = {},
        all = [], leveled = {};
  for( const chunk of chunks ) {

    // add local results to the global ones
    chunk.queryResult
      .forEach( (row) => {
        const key = hash( row );
        if( !(key in properties) ) {

          // create an entry
          const entry = {
            count: 0,
            level: row.level,
          };

          // add to all matching collections
          properties[ key ] = entry;
          all.push( entry );
          leveled[ row.level ] = leveled[ row.level ] || [];
          leveled[ row.level ].push( entry );

        }
        properties[ key ].count += row.withProp;
      });

    // calc the current stats
    const cutoff    = chunk.iriNumber * Config.minPredProb,
          predProp  = (el) => el.count > cutoff; 
    chunk.stats = {
      filteredCutoff: cutoff,
      raw: {
        all:  all.length,
      },
      filtered: {
        all:  all.filter( predProp ).length,
      },
    };
    Object.keys( leveled )
      .forEach( (level) => {
        chunk.stats.raw[ level ]      = leveled[ level ].length;
        chunk.stats.filtered[ level ] = leveled[ level ].filter( predProp ).length;
      });

    // remove intermediate properties
    delete chunk.iris;
    delete chunk.queryResult;

  } 


  // write results to console
  console.log( '' );
  console.table( chunks.map( (chunk) => {
    const out = {
      iriNumber: chunk.iriNumber,
    };
    const keys = Object.keys( chunk.stats.raw ).sort( (a,b) => (b.length - a.length) || a.localeCompare( b ) );

    // preserve some order in attributes
    for( const key of keys ) {
      const outKey = key == 'all' ? 'raw' : `raw_L${key}`;
      out[ outKey ]       = chunk.stats.raw[ key ];
    }
    for( const key of keys ) {
      const outKey = key == 'all' ? 'filtered' : `filtered_L${key}`;
      out[ outKey ]  = chunk.stats.filtered[ key ];
    }

    return out;
  } ) );
  await Fs.promises.writeFile( cfg.resFile, JSON.stringify( chunks, null, 2 ) );

  /* XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX UTIL FUNCTIONS XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX */

  /**
   * hash the given row
   */
  function hash( row ) {
    return cfg.hashProps
              .map( (p) => row[ p ] || '' )
              .join( '|' );
  }

  /**
   * process a single chunk of IRIs
   *
   * @param   {Array}     chunk       subset of IRIs
   * @returns {Array}                 query results for the given subset
   */
  async function processChunk( chunk ) {
  
    // collect the result
    const result = [];
    let formatedQuery, res, props;
  
    // query for direct (L0) properties
    // at this point we want all of them to make accurate counts
    if( containsResults( chunk, 0 ) ) {

      // reload existing results
      res = chunk.queryResult.filter( (row) => row.level == 0 );
      logger.info( `   (cached) level 0 candidates ${res.length}` );

    } else {
      
      // acquire new results
      formatedQuery = queryDirect( chunk.iris, 0 );
      res           = await conn( formatedQuery );
      logger.info( `   (retrieved) level 0 candidates ${res.length}` );
      res.forEach( processResultRow(0) );

    }
    result.push( ... res );

    // query for indirect properties
    for( let i=1;i<=cfg.intermediateHops; i++ ) {

      if( containsResults( chunk, i ) ) {
  
        // reload existing results
        res = chunk.queryResult.filter( (row) => row.level == i );
        logger.info( `   (cached) level ${i} candidates ${res.length}` );
  
      } else {
        
        // acquire new results

        // extract properties
        props = res
                 .filter( (row) => !row.dt )
                 .map( (row) => {
                   const res = [];
                   for( let j=1; j<=i; j++ ) {
                     res.push({ type: 'raw', value: row[`prop${j}`] });
                   }
                   return res;
                 });

        // run query
        res   = await execQuery( queryIndirectL2, chunk.iris, props, i, 0 );
        logger.info( `   (retrieved) level ${i} candidates ${res.length}` );
  
        // compress
        res.forEach( processResultRow(i) );

      }

      // add to result
      result.push( ... res );

    }

    // done
    return result;
  
  }
  
  
  /**
   * process each result row
   * - compress the results
   *
   * @param   {Number}      level       level of property path; number of intermediate hops
   */
  function processResultRow( level ){
    return function( row ) {

      // add the level
      row.level = level;

      // replace prefixes in some properties
      for( const key of cfg.compressProps ) {
        for( const prop of Object.keys( row ) ) {
          if( prop.startsWith( key ) ) {
            for( const iri of Object.keys( cfg.compressPrefix ) ) {
              row[ prop ] = row[ prop ].replace( iri, cfg.compressPrefix[ iri ] );
            }
          }
        }
      }

      // compress a little more
      delete row.probScore;
      row.dt = !!row.dt;

    };
  }


  /**
   * execute the query in chunks to prevent timeouts
   *
   * @param   {Function}      query       query to execute
   * @param   {Array}         iriChunk    one chunk or IRIs
   * @param   {Array}         props       property paths to use
   * @param   {...*}          args        other arguments to pass to the query function
   */
  async function execQuery( query, iriChunk, props, ...args ) {

    // split into props into chunks
    const propChunks = [];
    for( let i=0; i<props.length; i+=cfg.chunkSizeProp ){
      propChunks.push( props.slice( i, i + cfg.chunkSizeProp ) );
    }

    // execute queries
    const result = [],
          reqs   = [];
    for( const propChunk of propChunks ) {
      reqs.push((async function(){

        // get data
        const formatedQuery = query( iriChunk, propChunk, ... args );

        let data;
        try {
          data = await conn( formatedQuery );
        } catch( e ) {

          // try to extract the actual error message
          const splitError  = e.message.replace( formatedQuery, '' ).split( '\n' ), 
                customMsg   = splitError.length > 1 ? splitError[1] : splitError;
          throw new Error( customMsg );

        }

        // add data to result
        result.push( ... data );

      })());
    }

    await Promise.all( reqs );    
    return result;

  }
  
  /**
   * check, if the chunk has results for the given path size
   */
  function containsResults( chunk, hops ) {
    return ('queryResult' in chunk)
           && chunk.queryResult.some( (row) => row.level == hops );
  }

})().catch( (e) => console.error( e ) );


function delay( ms ) {
  return new Promise( (resolve) => setTimeout( resolve, ms ) );
}
