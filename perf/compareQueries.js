/*
 * compare two queries against each other
 * - runtime
 * - results
 */

// @TODO polyfill, might not be needed forever
import 'cross-fetch/polyfill';

// includes
import Config                 from '../src/config/config';
import iris                   from '../test/_fixtures/iris_novels';
import SpyConnection          from './trackQueries/SpyConnection';

// queries under test
import query0 from '../src/queries/getCandidates';
import query1 from '../src/queries/getCandidates_new';

(async function(){

  // configure call parameter of the queries
  iris.length = 1000;
  const queryParams = [
    iris    // input IRIs
      .map( (iri) => { return { type: 'iri', value: iri }; }),   
    0.1,    // minimum probability
  ];
  
  // insert a spy into the connection
  const spyConn = new SpyConnection( Config.endpoint ),
        execQuery = spyConn.getConnection();

  // prepare queries
  const formatedQuery0 = `#${Date.now()}\n` + query0( ... queryParams ),
        formatedQuery1 = `#${Date.now()}\n` + query1( ... queryParams );

  // run the queries
  const res0 = await execQuery( formatedQuery0 );
  await delay( 5 * 1000 );
  const res1 = await execQuery( formatedQuery1 );

  // print the query report report
  console.log( `query parameters used: ${JSON.stringify( queryParams )}` );
  console.log( '' );
  spyConn.printReport();
  
  // XXXXXXXXXXXXXXXXXXXXXX Result Comparison XXXXXXXXXXXXXXXXXXXXXX

  // get keys used
  const keys = [ ... new Set([
    res0.map( (row) => Object.keys( row ) ),
    res1.map( (row) => Object.keys( row ) ),
  ].flat( 2 ) )];

  // remember, where we got each row from
  const origin = Symbol.for( 'source of row' );
  res0.forEach( (row) => row[ origin ] = 0 );
  res1.forEach( (row) => row[ origin ] = 1 );

  // sort both inputs accordingly
  const sorter = (a,b) => {
    for( const key of keys ) {
      if( a[ key ] != b[ key ] ) {
        return ('' + a[ key ]).localeCompare( '' + b[ key ] );
      }
    }
    return 0;
  };
  const sorted = [ ... res0, ... res1 ].sort( sorter );

  // create diff
  const diff = [ [], [] ];
  let i = 0;
  while( i<sorted.length ) {
    
    if( sorter( sorted[i], sorted[i+1] || {} ) == 0 ) {

      // both are the same, so everything is in order and we go to the next pair
      i += 2;

    } else {

      // we found a difference, so remember it and just proceed one step
      diff[ sorted[i][ origin ] ].push( sorted[i] );
      i += 1;
      
    }
    
  }
  
  // write report
  console.log( '' );
  console.log( `rows omitted: ${diff[0].length}` );
  console.log( `rows added:   ${diff[1].length}` );

  console.log( '\nomitted rows:' );
  console.log( diff[0].map( (r) => JSON.stringify( r ) ) );

  console.log( '\nadded rows:' );
  console.log( diff[1].map( (r) => JSON.stringify( r ) ) );

})().catch( (e) => console.error( e ) );



function delay( ms ) {
  return new Promise( (resolve) => setTimeout( resolve, ms ) );
}
