/*
 * tracking script to evaluate the number of queries fired throughout the facet generation process
 * - no warmup phase
 */

// @TODO polyfill, might not be needed forever
import 'cross-fetch/polyfill';

// includes
import Time                   from 'microtime';
import getFacets              from '../src/index';
import Config                 from '../src/config/config';
import iris                   from '../test/_fixtures/iris_novels';
import SpyConnection          from './trackQueries/SpyConnection';

(async function(){

  // configure
  const cfg = {
    numberIRI: 10,   // number of IRIs to use
  };

  // extract IRI subset
  iris.length = cfg.numberIRI;
  
  // insert a spy into the connection
  const spyConn = new SpyConnection( Config.endpoint );

  // run the queries
  const start = Time.now();
  const res = await getFacets( spyConn.getConnection(), iris, 5 );
  const end = Time.now();

  // print the report
  spyConn.printReport();
  console.log( `\noverall time spent: ${end-start}` );
  console.log( `sample IRIs used: ${cfg.numberIRI}` );

})().catch( (e) => console.error( e ) );
