/*
 * test semantic similarity for given tests to derive threshold
 */

// @TODO polyfill, might not be needed forever
import 'cross-fetch/polyfill';

// includes
import 'console.table';
import Config                 from '../src/config/config';
import Facet                  from '../src/objects/facet';
import queryLabels            from './../src/queries/labels';
import createConnectionPool   from './../src/util/ThrottledRequests';
import semanticSimilarity     from './../src/scores/semanticSimilarity';
import prepare                from './../src/scores/semanticSimilarity/prepare';

(async function(){

  // define testcases
  const prefix = 'http://www.wikidata.org/entity/',
        tests = [

          // directly connected with one subclass
          { ent1: 'Q2500638' /* creator */,               ent2: 'Q215627' /* person */ },
          // connected with 2 subclasses
          { ent1: 'Q3630699' /* game designer */,         ent2: 'Q215627' /* person */ },
          // connected with 3 subclasses
          { ent1: 'Q3630699' /* game designer */,         ent2: 'Q830077' /* subject */ },
          // connected with one instance of
          { ent1: 'Q3630699' /* game designer */,         ent2: 'Q28640' /* profession */ },
          // connected with instanceOf-subclassOf
          { ent1: 'Q3630699' /* game designer */,         ent2: 'Q12737077' /* occupation */ },
          // connected with instanceOf-instanceOf
          { ent1: 'Q3630699' /* game designer */,         ent2: 'Q19361238' /* Wikidata meta class */ },

          // various other pairs
          { ent1: 'Q36834' /* composer */,                ent2: 'Q36180' /* writer */ },
          { ent1: 'Q36834' /* composer */,                ent2: 'Q28389' /* screenwriter */ },
          { ent1: 'Q36834' /* composer */,                ent2: 'Q20002503' /* story artist */ },
          { ent1: 'Q4087517' /* beatmaker */,             ent2: 'Q28389' /* screenwriter */ },
          { ent1: 'Q28039322' /* story supervisor */,     ent2: 'Q36834' /* composer */ },

          // similarity in film scenario
          { ent1: 'Q1361758' /* publication date */,      ent2: 'Q24575110' /* start time */ },
          // counterexample
          { ent1: 'Q1207505' /* has quality */,           ent2: 'Q2199864' /* duration */ },

          // evaluation scenario
          { ent1: 'Q6256' /* country */,                  ent2: 'Q5107' /* continent */ },
          // film scenario
          { ent1: 'Q6256' /* country */,                  ent2: 'Q19502208' /* geographic sitting of film */ },
          { ent1: 'Q34770' /* language */,                ent2: 'Q21707202' /* original language */ },
          { ent1: 'Q3455803' /* director */,              ent2: 'Q12540664' /* distributor */ },
          { ent1: 'Q3455803' /* director */,              ent2: 'Q36834' /* composer */ },
          { ent1: 'Q2199864' /* duration */,              ent2: 'Q24575125' /* end time */ },
          { ent1: 'Q1361758' /* publication date */,      ent2: 'Q24575125' /* end time */ },
          { ent1: 'Q23649977' /* FSK rating category */,  ent2: 'Q23830574' /* Filmiroda rating system */ },
          // novel scenario
          { ent1: 'Q482980' /* author */,                 ent2: 'Q2085381' /* publisher */ },
          { ent1: 'Q1361758' /* publication date */,      ent2: 'Q24574747' /* inception */ },
          { ent1: 'Q15989253' /* part */,                 ent2: 'Q24575087' /* has part */ },
          { ent1: 'Q482980' /* author */,                 ent2: 'Q95074' /* fictional character */ },
          // counterexamples should definitely not be similar
          { ent1: 'Q483394' /* genre */,                  ent2: 'Q3373417' /* country of origin */ },
          { ent1: 'Q482980' /* author */,                 ent2: 'Q836950' /* derivative work */ },

        ];

  // create connection
  const conn = createConnectionPool( Config.endpoint );

  // calculate similarities
  await Promise.all( tests.map( (t) => getSimilarity( conn, prefix, t ) ) );

  // get labels for all entities
  const inputLabels = tests.map( (t) => [ t.ent1, t.ent2 ] )
                           .flat( 1 )
                           .map( (iri) => ({ type: 'iri', value: prefix + iri }) );

  // retrieve labels
  const formatedQuery = queryLabels( inputLabels ),
        labels        = (await conn( formatedQuery ))
                          .reduce( (all,row) => {
                            all[ row.iri ] = row.iriLabel;
                            return all;
                          }, {});
  tests.forEach( (t) => {
    t.label1 = labels[ prefix + t.ent1 ];
    t.label2 = labels[ prefix + t.ent2 ];
  });

  // prepare result table
  const out = tests.map( (t) => ({
    ent1: `${t.label1} (${t.ent1})`,
    ent2: `${t.label2} (${t.ent2})`,
    sim:  t.sim,
  }) );

  // print
  console.table( out );

})().catch( (e) => console.error( e ) );


/**
 * fake two facet with the respective properties and calculate the similarity
 */
async function getSimilarity( conn, prefix, test ) {

  // create facets
  const f1 = new Facet( `http://fake.example.org/${test.ent1}` ),
        f2 = new Facet( `http://fake.example.org/${test.ent1}` );

  // set entities
  f1.path[0].entity = prefix + test.ent1;
  f2.path[0].entity = prefix + test.ent2;

  // run similarity calculation
  await Promise.all([
    prepare( conn, f1 ),
    prepare( conn, f2 ),
  ]);
  const score = await semanticSimilarity( conn, [ f1 ], f2 );

  // attach to test
  test.sim = score;

}
